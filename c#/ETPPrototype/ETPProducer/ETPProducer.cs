﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
//
// Use the WebSocket4Net library for a client/producer
//
using WebSocket4Net;

//
// These are from the Apache Avro DLL
//
using Avro;
using Avro.IO;
using Avro.File;
using Avro.Generic;
using Avro.Specific;
//
// These are from ETP.dll containing classes derived from the ETP JSON schemas
//
using Energistics.Datatypes;
using Energistics.Datatypes.ChannelData;
using Energistics.Protocol.Core;
using Energistics.Protocol.ChannelStreaming;

//
// Simple Application to connect to a WebSocket server as a data producer and send ETP Avro messages
//
// The sequence is:
// This Application (producer)                          ETP Server
// ----------------                                     ---------------
//                                                      Start WebSocket Server
// Send RequestSession
//                                                      Send OpenSession             
//                                                      Send Start Message
// Send ChannelMetadata
//                                                      Send ChannelStreamingStart  
// Send ChannelData in a loop thread
//
class ETPProducer
{
    private static long _counter = 0; // Message counter
    private static WebSocket _websocket = null;
    private XmlNodeList _logCurveInfo; // Used to hold metadata from <logCurveInfo> elements in a WITSML log data file
    private XmlNodeList _data; // Used to hold the <data> elements from a WITSML log data file
    private DataThread _dataThread = null;
    private Thread _thread = null;

    static void Main(string[] args)
    {   
        new ETPProducer().connect();
        Console.WriteLine("Press any key to exit...");
        Console.ReadKey();
        SendCloseSession();
        _websocket.Close("ETPProducer Terminated");
    }

    private void connect()
    {
        LoadData(); // Pre-loads the WITSML data file "gentime.xml"
        var l = new List<KeyValuePair<string, string>>();
        l.Add(new KeyValuePair<string, string>("etp-encoding", "binary"));    // Set up the additional ETP parameter(s)
        _websocket = new WebSocket("ws://localhost:8080", "", null, l); // Configure the connection to the WebSocket server
        _websocket.Opened += new EventHandler(websocket_Opened);
        _websocket.Error += new EventHandler<SuperSocket.ClientEngine.ErrorEventArgs>(websocket_Error);
        _websocket.Closed += new EventHandler(websocket_Closed);
        _websocket.MessageReceived += new EventHandler<MessageReceivedEventArgs>(websocket_MessageReceived);
        _websocket.DataReceived += new EventHandler<DataReceivedEventArgs>(websocket_DataReceived);
        _websocket.Open();
    }

    //  
    //
    // When the WebSocket is opened, send a RequestSession message
    //
    private void websocket_Opened(object sender, EventArgs e)
    {
        Console.WriteLine("WebSocket Opened");
        // Create an ETP MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 0;
        mh.MessageType = 1; // RequestSession message type
        mh.MessageId = _counter++;

        // Create a list (of 1) SupportedProtocol object for a consumer of protocol 1 data
        var sp = new SupportedProtocol();
        sp.Protocol = 1;
        sp.Role = @"producer";
        var dict = new Dictionary<string, DataValue>(); // This is for a list of protocol Capabilities, if required. In this case it is empty
        sp.ProtocolCapabilities = dict;
        var v = new Energistics.Datatypes.Version();
        v.Major = 1;
        v.Minor = 0;
        v.Revision = 0;
        v.Patch = 0;
        sp.ProtocolVersion = v;
        var lsp = new List<SupportedProtocol>(); // Create a list of supported protocol objects to populate the RequestSession message
        lsp.Add(sp);
        // Create an ETP RequestSession message
        var rs = new RequestSession();
        rs.ApplicationName = @"ETPProducer"; // The name of this application
        rs.RequestedProtocols = lsp; // Add list of requested protocols

        // Now write the MessageHeader and the RequestSession messages to the WebSocket
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<RequestSession>(rs.Schema);
            bodyWriter.Write(rs, encoder);
            byte[] ba = ms.ToArray();
            _websocket.Send(ba, 0, ba.Length);
        }
        // This will trigger the server to send an OpenSession message which will be received via the websocket_DataReceived() method below
    }
    //
    // This is called when the WebSocket is closed
    //
    private void websocket_Closed(object sender, EventArgs e)
    {
        Console.WriteLine("WebSocket Closed");
        CloseThread();
    }

    void websocket_DataReceived(object sender, DataReceivedEventArgs e)
    {
        Console.WriteLine("Message received: length: " + e.Data.Length);
        var mlist = new List<MemoryStream>();
        //Console.WriteLine("Create Memory Stream");
        mlist.Add(new MemoryStream(e.Data));
        //Console.WriteLine("After Create Memory Stream");
        var bis = new ByteBufferInputStream(mlist);
        //Console.WriteLine("After Create Input Stream");
        var decoder = new BinaryDecoder(bis); // This can be re-used for all arriving objects
        //Console.WriteLine("After Create Decoder");
        var mh = new MessageHeader();
        //Console.WriteLine("Read message header");
        var headerReader = new SpecificReader<MessageHeader>(mh.Schema, mh.Schema);
        headerReader.Read(mh, decoder);
        Console.WriteLine("Header Message: Protocol: " + mh.Protocol + " Message Type: " + mh.MessageType + " Message Id: " + mh.MessageId);
        switch (mh.Protocol)
        {
            case 0: // Core Protocol Messages
                switch (mh.MessageType)
                {
                    case 1: // Request session - don't need to support this message in a producer
                        break;
                    case 2: // Open Session
                        var os = new OpenSession();
                        var osReader = new SpecificReader<OpenSession>(os.Schema, os.Schema);
                        osReader.Read(os, decoder);
                        ProcessOpenSession(os);
                        break;
                    case 5: // Close the session. Need to decode the CloseSession message here and free resources, if necessary
                        var cs = new CloseSession();
                        var csReader = new SpecificReader<CloseSession>(cs.Schema, cs.Schema);
                        csReader.Read(cs, decoder);
                        Console.WriteLine("ETP close session - reason "+cs.Reason);
                        break;
                }
                break;
            case 1: // Streaming protocol messages
                switch (mh.MessageType)
                {
                    case 0: // Start message with throttling parameters, if required. No response required
                        var st = new Start();
                        var stReader = new SpecificReader<Start>(st.Schema, st.Schema);
                        stReader.Read(st, decoder);
                        Console.WriteLine("Start Message: Max Data Items: " + st.MaxDataItems + " Max Message Rate: " + st.MaxMessageRate);
                        SendMetadata();
                        break;
                    case 1: // Channel Describe message 
                        // Should not get this message in a Simple Producer
                        break;
                    case 2: // Channel meta data
                        // Should not get this message in a Simple Producer
                        break;
                    case 3: // Channel data
                        // Should not get this message in a Simple Producer
                        break;
                    case 4: // ChannelStreamingStart message
                        var cs = new ChannelStreamingStart();
                        var csReader = new SpecificReader<ChannelStreamingStart>(cs.Schema, cs.Schema);
                        csReader.Read(cs, decoder);
                        StartStreaming(cs);
                        break;
                    case 5: // ChannelStreamingStop message
                        var css = new ChannelStreamingStop();
                        var cssReader = new SpecificReader<ChannelStreamingStop>(css.Schema, css.Schema);
                        cssReader.Read(css, decoder);
                        StopStreaming(css);
                        break;
                }
                break;
        }
    }
    //
    // This is for text messages received
    //
    private void websocket_MessageReceived(object sender, EventArgs e)
    {
        Console.WriteLine("Text Message Received: " + e.ToString());
    }

    private void websocket_Error(object sender, EventArgs e)
    {
        Console.WriteLine("Error: " + e.ToString());
    }


    //
    // Process an OpenSession message and send out a Start message in response
    //
    private void ProcessOpenSession(OpenSession os)
    {
        Console.WriteLine("OpenSession:  " + os.ApplicationName + " sessionId: " + os.SessionId);
    }

    //
    // Respond to a Start message - send out a ChannelMetadata message 
    //
    private void SendMetadata()
    {
        // Send a MessageHeader, followed by ChannelMetadata Message
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 1; // Streaming protocol
        mh.MessageType = 2; // ChannelMetadata message
        mh.MessageId = _counter++;
        // Create a ChannelMetadata message
        var cm = new ChannelMetadata();
        // Each ChannelMetadata DataItem has a list of IndexMetaDataRecords 
        // For this application, we assume a single time index
        var imr = new IndexMetadataRecord();
        imr.Datum = null;
        imr.Direction = IndexDirections.Increasing;
        imr.IndexType = ChannelIndexTypes.Time;
        imr.Uom = "s";
        var il = new List<IndexMetadataRecord>();
        il.Add(imr);
        // Create a list of ChannelMetadataRecords
        var cl = new List<ChannelMetadataRecord>();
        for(int i=0;i<_logCurveInfo.Count;i++)
        {
            var n = _logCurveInfo[i];
            var cmr = new ChannelMetadataRecord();
            cmr.ChannelId = i;
            cmr.Mnemonic = GetXmlNodeValue(n, "mnemonic");
            cmr.ChannelUri = cmr.Mnemonic;
            cmr.DataType = GetXmlNodeValue(n, "typeLogData");
            cmr.Description = GetXmlNodeValue(n,"curveDescription");
            cmr.Uom = GetXmlNodeValue(n,"unit");
            cmr.StartIndex = null;
            cmr.EndIndex = null;
            cmr.Status = ChannelStatuses.Active;
            cmr.Indexes = il;
            cl.Add(cmr);
        }
        cm.Channels = cl;
        // Now write the messages to the WebSocket. 
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<ChannelMetadata>(cm.Schema);
            bodyWriter.Write(cm, encoder);
            byte[] ba = ms.ToArray();
            Console.WriteLine("Send ChannelMetadata - length: "+ba.Length);
            _websocket.Send(ba, 0, ba.Length);
        }
    }
    /// <summary>
    /// Close the WebSocket session
    /// </summary>
    private static void SendCloseSession()
    {
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 0; // Core protocol
        mh.MessageType = 5; // CloseSession message
        mh.MessageId = _counter++;

        // Create a ChannelStreamingStop message
        var cs = new CloseSession();
        cs.Reason = @"Application ETPConsumer terminated";
        // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<CloseSession>(cs.Schema);
            bodyWriter.Write(cs, encoder);
            byte[] ba = ms.ToArray();
            _websocket.Send(ba, 0, ba.Length);
        }
    }

    //
    // Start Streaming Channel data in a new Thread, using the DataThread sub-class
    //
    private void StartStreaming(ChannelStreamingStart cs)
    {
        Console.WriteLine("StartStreaming:");
        if (_dataThread == null)
        {
            _dataThread = new DataThread();
            _dataThread.Setup(_websocket, _data, _logCurveInfo, _counter);
            _thread = new Thread(new ThreadStart(_dataThread.Run));
            _thread.Start();
        }
        _dataThread.Interrupt(false);
        
    }

    //
    // Stop Streaming Channel data 
    //
    private void StopStreaming(ChannelStreamingStop cs)
    {
        Console.WriteLine("StopStreaming:");
        _dataThread.Interrupt(true);
    }

    private void CloseThread()
    {
        if (_dataThread != null)
        {
            _dataThread.Terminate();
        }
    }

    //
    // Load a WITSML log file into memory for serving up data
    //
    private void LoadData()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load("gentime.xml");
        XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
        nsmgr.AddNamespace("w", "http://www.witsml.org/schemas/131");
        _logCurveInfo = doc.SelectNodes("//w:logCurveInfo",nsmgr);
        _data = doc.SelectNodes("//w:data",nsmgr);
        Console.WriteLine("<logCurveInfo> nodes " + _logCurveInfo.Count);
        Console.WriteLine("<data> nodes "+_data.Count);
        Console.WriteLine("Data Loaded");
    }
    //
    // Get the value of an XML node one level deeper than the start node 
    // Used to extract the <logCurveInfo> data 
    //
    private static string GetXmlNodeValue(XmlNode n, string subNode)
    {
        if (n[subNode] != null)
        {
            return (n[subNode].InnerText);
        }
        return ("");
    }
    //
    // This class goes into a loop, sending channel data extracted from a WITSML log data file
    //
    private class DataThread
    {
        private WebSocket _websocket;
        private XmlNodeList _data;
        private XmlNodeList _logCurveInfo;
        private long _counter = 0;
        private int _dataCount = 0;
        private bool _interrupted = false;
        private bool _exit = false;
        private int _status = 0;

        public void Setup(WebSocket ws, XmlNodeList data,XmlNodeList lci,long count)
        {
            _websocket = ws;
            _data = data;
            _logCurveInfo = lci;
            _counter = count;
        }
        //
        // Used to control the Thread
        // 
        public void Interrupt(bool interrupt)
        {
            _interrupted = interrupt;
        }

        public void Terminate()
        {
            _exit = true;
        }

        public int GetStatus()
        {
            return (_status);
        }

        public void Run()
        {
            // Get an Array of DataValue types from the <logCurveInfo> metadata
            string[] dataTypes = new string[_logCurveInfo.Count];
            for (int i = 0; i < _logCurveInfo.Count; i++)
            {
                dataTypes[i] = GetXmlNodeValue(_logCurveInfo[i], "typeLogData");
                if (dataTypes[i].Equals("")) dataTypes[i] = "double";
            }
            _status = 1;
            Console.Write("Looping.");
            while (!_exit)
            {
                if (!_interrupted)
                {
                    Console.Write(".");
                    // Send a MessageHeader, followed by ChannelMetadata Message
                    // Create a MessageHeader message
                    var mh = new MessageHeader();
                    mh.Protocol = 1; // Streaming protocol
                    mh.MessageType = 3; // ChannelData message
                    mh.MessageId = _counter++;
                    //
                    // Parse the WITSML log data and build up a ChannelData record
                    if (_dataCount >= _data.Count) _dataCount = 0; // Loop round the data file
                    XmlNode n = _data[_dataCount++];
                    var s = n.InnerText; // Content of <data> element from WITSML log
                    char[] sc = { ',' }; // Separator character
                    var sa = s.Split(sc); // String array of data values
                    var ldi = new List<DataItem>(); // ChannelData contains a list of DataItems
                    var udt = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
                    for (int i = 0; i < sa.Length; i++)
                    {
                        // Create a time index with the current date/time (ignore the one from the WITSML log file)
                        var timeval = System.DateTime.UtcNow.Ticks / 10 - udt.Ticks/10; // Current time in microseconds referenced to 1970
                        var dt = new Energistics.Datatypes.DateTime();
                        dt.Time = timeval;
                        dt.Offset = 0;
                        var idx = new IndexValue();
                        idx.Item = dt;
                        var li = new List<IndexValue>();
                        li.Add(idx);
                        // Now decode the data value into the appropriate data type
                        var dv = new DataValue();
                        dv.Item = 0.0; // Put in a default value
                        if (!sa[i].Equals("") && !sa[i].ToLower().Equals("null"))
                        {
                            if (dataTypes[i].Equals("double"))
                            {
                                dv.Item = Double.Parse(sa[i]);
                            }
                            else if (dataTypes[i].Equals("float"))
                            {
                                dv.Item = Single.Parse(sa[i]);
                            }
                            else if (dataTypes[i].Equals("long"))
                            {
                                dv.Item = Int64.Parse(sa[i]);
                            }
                            else if (dataTypes[i].Equals("date time"))
                            {
                                System.DateTime dtim = ETPUtil.ParseW3CDate(sa[i]); // Convert WITSML W3C date/time to a System DateTime object here
                                var edt = new Energistics.Datatypes.DateTime();
                                edt.Time = dtim.Ticks / 10 - udt.Ticks / 10 ; // Time in microseconds from 1970
                                dv.Item = edt;
                            }
                            else if (dataTypes[i].ToLower().Equals("string"))
                            {
                                dv.Item = sa[i];
                            }
                        }
                        // Add the channel id, index and data value to a DataItem
                        var di = new DataItem();
                        di.ChannelId = i;
                        di.Indexes = li; // This could be set to null for values after the first one if they are all at the same index
                        di.Value = dv;
                        di.ValueAttributes = new List<DataAttribute>(); // just add an empty list here
                        ldi.Add(di);
                    }
                    var cd = new ChannelData();
                    cd.Data = ldi; // List of DataItems
                    // Now write the messages to the WebSocket. 
                    using (var ms = new MemoryStream())
                    {
                        var encoder = new BinaryEncoder(ms);
                        var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
                        headerWriter.Write(mh, encoder);
                        var bodyWriter = new SpecificWriter<ChannelData>(cd.Schema);
                        bodyWriter.Write(cd, encoder);
                        byte[] ba = ms.ToArray();
                        _websocket.Send(ba, 0, ba.Length);
                    }
                }
                Thread.Sleep(1000);
            }
            Console.WriteLine("DataThread terminated");
            _status = 0;
        }
    }
}
