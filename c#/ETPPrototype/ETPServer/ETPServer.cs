﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using System.Diagnostics;
//
// Use the SuperWebSocket library for the server
//
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using SuperWebSocket;

//
// These are from the Apache Avro DLL
//
using Avro;
using Avro.IO;
using Avro.File;
using Avro.Generic;
using Avro.Specific;
//
// These are from ETP.dll containing classes derived from the ETP JSON schemas
//
using Energistics.Datatypes;
using Energistics.Datatypes.ChannelData;
using Energistics.Protocol.Core;
using Energistics.Protocol.ChannelStreaming;

/// <summary>
/// Simple WebSocket server to consume and produce ETP messages
/// This server accepts connections from simple producers and consumers of ETP protocol 1 streaming channel data.
/// It does not support subscriptions to specific channels. Each consumer gets all channel data.
/// In this prototype, only one connected producer is supported but multiple consumers may attach.
///
/// For an ETP Consumer, the sequence is:
/// This Application (server)                            ETP Consumer
/// ----------------                                     ---------------
/// Start WebSocket Server  
///                                                      Send RequestSession
/// Send OpenSession             
///                                                      Send Start Message
///                                                      Send ChannelDescribe
/// Send ChannelMetadata
///                                                      Send ChannelStreamingStart  
/// Send ChannelData as received from producers
///
/// For an ETP Producer, the sequence is:
/// This Application (server)                            ETP Producer
/// ----------------                                     ---------------
/// Start WebSocket Server  
///                                                      Send RequestSession
/// Send OpenSession             
/// Send Start Message
///                                                      Send ChannelMetadata
/// Send ChannelStreamingStart  
///                                                      Send ChannelData in a loop thread
/// </summary>
class ETPServer
{
    // This is a single structure for all instances of this class to hold the global list of connected sessions
    private static Dictionary<string, ETPSession> _sessions = new Dictionary<string, ETPSession>();

    // The variables below are used for each instance of the ETPServer class
    private long _counter = 0; // Message counter
    SuperWebSocket.WebSocketServer _ws;
    private ETPSession _curSession = null;
    private ChannelMetadata _metadata = null;

    static void Main(string[] args)
    {
        new ETPServer().startup();
        Console.WriteLine("Press any key to exit...");
        Console.ReadKey();
    }

    /// <summary>
    /// Start up the WebSocket server and set up the callback methods
    /// </summary>
    private void startup()
    {
        var s = new SuperSocket.SocketBase.Config.ServerConfig();
        s.Name = "ETPServer";
        s.Ip = "Any";
        s.Port = 8080;
        s.Mode = SocketMode.Tcp;
        // Set the maximum size of messages to be transferred
        s.MaxRequestLength = 2 * 1024 * 1024; // 2MB message size
        s.ReceiveBufferSize = s.MaxRequestLength;

        /* For future use to implement security stuff
         s.Mode = SocketMode.Sync;
         s.Security = "tls";
         var cert = new SuperSocket.SocketBase.Config.CertificateConfig();
         cert.FilePath = @"C:\SSL\yourhostname.cer";
         cert.Password = "";
         cert.IsEnabled = true;
         s.Certificate = cert;
         */
         
        _ws = new WebSocketServer();
        _ws.Setup(s);
        _ws.NewSessionConnected += ws_NewSessionConnected;
        _ws.NewMessageReceived += ws_NewMessageReceived;
        _ws.NewDataReceived += ws_NewDataReceived;
        _ws.SessionClosed += ws_SessionClosed;
        _ws.Start();
        Console.WriteLine("WebSocket Server Started at ws://localhost:"+s.Port);
    }
 
    /// <summary>
    /// A new session has connected. 
    /// Need to cache session specific information here to implement a multi-connection server
    /// </summary>
    /// <param name="session"></param>
    void ws_NewSessionConnected(WebSocketSession session)
    {
        Console.WriteLine("New Session connected - session Id:" + session.SessionID);
        // session.SendResponse("Hello From C#!");
        _curSession = new ETPSession(); // Create an ETPSession object to hold session-specific data
        _curSession.id = session.SessionID;
        _sessions.Add(_curSession.id, _curSession); // Add to global Dictionary of connected sessions
    }

    /// <summary>
    /// A session has closed
    /// Need to free up resources here
    /// </summary>
    /// <param name="session"></param>
    /// <param name="e"></param>
    void ws_SessionClosed(WebSocketSession session, CloseReason e)
    {
        // Need to de-allocate the session data in here
        Console.WriteLine("Session closed - reason: " + e.ToString());
        _sessions.Remove(session.SessionID); // Remove from global dictionary of sessions
    }
    /// <summary>
    /// Called when new binary data arrives on a WebSocket session
    /// This is the main processing part of the server. Each message starts with an ETP MessageHeader.
    /// The MessageHeader is decoded to determine the protocol and message number for the following message which is
    /// processed in the switch statements, according to the protocol and message type
    /// </summary>
    /// <param name="session">The sending session</param>
    /// <param name="e">A byte array of data</param>
    void ws_NewDataReceived(WebSocketSession session, byte[] e)
    {
        Console.WriteLine("Message received: length: " + e.Length);
        var mlist = new List<MemoryStream>();
        //Console.WriteLine("Create Memory Stream");
        mlist.Add(new MemoryStream(e));
        //Console.WriteLine("After Create Memory Stream");
        var bis = new ByteBufferInputStream(mlist);
        //Console.WriteLine("After Create Input Stream");
        var decoder = new BinaryDecoder(bis); // This can be re-used for all arriving objects
        //Console.WriteLine("After Create Decoder");
        var mh = new MessageHeader();
        //Console.WriteLine("Read message header");
        var headerReader = new SpecificReader<MessageHeader>(mh.Schema, mh.Schema);
        headerReader.Read(mh, decoder);
        Console.WriteLine("Header Message: Protocol: " + mh.Protocol + " Message Type: " + mh.MessageType + " Message Id: " + mh.MessageId);
        ETPSession es = _sessions[session.SessionID];
        switch (mh.Protocol)
        {
            case 0: // Core Protocol Messages
                switch (mh.MessageType)
                {
                    case 1: // Request session - send an Open Session message back
                        var rs = new RequestSession();
                        var rsReader = new SpecificReader<RequestSession>(rs.Schema, rs.Schema);
                        rsReader.Read(rs, decoder);
                        ProcessRequestSession(session, rs);
                        break;
                    case 2: // Open Session - should not be received by an ETP Server
                        break;
                    case 5: // Close the session. Need to decode the CloseSession message here and free resources, if necessary
                        var cs = new CloseSession();
                        var csReader = new SpecificReader<CloseSession>(cs.Schema,cs.Schema);
                        csReader.Read(cs,decoder);
                        Console.WriteLine("ETP close session from: "+es.appName+" reason: "+cs.Reason);
                        _sessions.Remove(session.SessionID);
                        break;
                }
                break;
            case 1: // Streaming protocol messages
                switch (mh.MessageType)
                {
                    case 0: // Start message with throttling parameters, if required. No response required
                        var st = new Start();
                        var stReader = new SpecificReader<Start>(st.Schema, st.Schema);
                        stReader.Read(st, decoder);
                        Console.WriteLine("Start Message: Max Data Items: " + st.MaxDataItems + " Max Message Rate: " + st.MaxMessageRate);
                        break;
                    case 1: // Channel Describe message - need to respond with ChannelMetadata
                        var cd = new ChannelDescribe();
                        var cdReader = new SpecificReader<ChannelDescribe>(cd.Schema,cd.Schema);
                        cdReader.Read(cd, decoder);
                        ProcessChannelDescribe(session, cd);
                        break;
                    case 2: // Channel meta data
                        var cm = new ChannelMetadata();
                        var cmReader = new SpecificReader<ChannelMetadata>(cm.Schema,cm.Schema);
                        cmReader.Read(cm, decoder);
                        ProcessChannelMetadata(session, cm);
                        break;
                    case 3: // Channel data
                        // Read in the channel data and send it out to any subscribed consumers
                        var cda = new ChannelData();
                        var cdaReader = new SpecificReader<ChannelData>(cda.Schema,cda.Schema);
                        cdaReader.Read(cda, decoder);
                        ProcessChannelData(session, cda);
                        break;
                    case 4: // ChannelStreamingStart message
                        var cs = new ChannelStreamingStart();
                        // This simple server does not need the info in the ChannelStreamingStart message but this is how to read it anyway
                        // For a more advanced server, individual channels may be started from a specified index
                        // var csReader = new SpecificReader<ChannelStreamingStart>(cs.Schema, cs.Schema); 
                        // csReader.Read(cs, decoder);
                        es.interrupted = false; // Re-start the current session if it is a consumer
                        break;
                    case 5: // ChannelStreamingStop message
                        var css = new ChannelStreamingStop();
                        // This simple server does not need the info in the ChannelStreamingStop message but this is how to read it anyway
                        // For a more advanced server, individual channels may be stopped
                        // var cssReader = new SpecificReader<ChannelStreamingStop>(css.Schema, css.Schema);
                        // cssReader.Read(css, decoder);
                        es.interrupted = true; // Interrupt the current session if it is a consumer
                        break;
                }
                break;
        }
    }

    /// <summary>
    /// Text message are processed here
    /// </summary>
    /// <param name="session">the sending session</param>
    /// <param name="e">The text of the message</param>
    void ws_NewMessageReceived(WebSocketSession session, string e)
    {
        Console.WriteLine("Text message received: " + e);
        // session.Send("Message: " + e);
    }

    /// <summary>
    /// Process a RequestSession message and send out an OpenSession message in response
    /// </summary>
    /// <param name="session">the session object from the message sender</param>
    /// <param name="rs">the RequestSession message</param>
    private void ProcessRequestSession(WebSocketSession session, RequestSession rs)
    {
        Console.WriteLine("RequestSession: App Name: " + rs.ApplicationName + " session: "+session.SessionID+" Requested protocol: " + rs.RequestedProtocols[0].Protocol);
        _curSession.appName = rs.ApplicationName;
        _curSession.role = rs.RequestedProtocols[0].Role; // Get the application consumer or producer role
        var protocol = rs.RequestedProtocols[0].Protocol;

        // Send a MessageHeader, followed by an OpenSession Message
        // Create a MessageHeader message
        var mh = new MessageHeader();
        // Console.WriteLine("Message header created");
        mh.Protocol = 0; // Core protocol
        mh.MessageType = 2; // OpenSession message
        mh.MessageId = _counter++;
        // Create an OpenSession message
        var os = new OpenSession();
        os.ApplicationName = @"ETPServer";
        os.SessionId = session.SessionID;
        // Create a list of 2 SupportedProtocol objects for a producer and consumer of protocol 1 data
        var sp = new SupportedProtocol();
        sp.Protocol = 1;
        sp.Role = @"producer";
        // Add the "protocolVersion" element to SupportedProtocol
        var v = new Energistics.Datatypes.Version();
        v.Major = 1;
        v.Minor = 0;
        v.Revision = 0;
        v.Patch = 0;
        sp.ProtocolVersion = v;
        // Add the "protocolCapabilities" element to SupportedProtocol
        var dict = new Dictionary<string, DataValue>(); // This is for a list of protocol Capabilities, if required. In this case it is empty
        sp.ProtocolCapabilities = dict;
        var lsp = new List<SupportedProtocol>(); // Create a list of supported protocol objects to populate the OpenSession message
        lsp.Add(sp);
        // Now add the second SupportedProtocol
        sp = new SupportedProtocol();
        sp.Protocol = 1;
        sp.Role = @"consumer";
        // Add the "protocolVersion" element to SupportedProtocol
        sp.ProtocolVersion = v;
        // Add the "protocolCapabilities" element to SupportedProtocol
        sp.ProtocolCapabilities = dict;
        lsp.Add(sp); // Add the second SupportedProtocol to the list
        os.SupportedProtocols = lsp;
        // Now write the messages to the WebSocket. 
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<OpenSession>(os.Schema);
            bodyWriter.Write(os, encoder);
            byte[] ba = ms.ToArray();
            Console.WriteLine("Send OpenSession");
            session.Send(ba, 0, ba.Length);
        }
        //
        // If the RequestSession was for Protocol 1, send a Start message
        //
        if (protocol != 1) return;
        // Create a MessageHeader message
        mh = new MessageHeader();
        mh.Protocol = 1; // Streaming protocol
        mh.MessageType = 0; // Start message
        mh.MessageId = _counter++;
        // Create a Start message
        var ss = new Start();
        ss.MaxDataItems = 0;
        ss.MaxMessageRate = 0;
        // Now write the messages to the WebSocket. 
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<Start>(ss.Schema);
            bodyWriter.Write(ss, encoder);
            byte[] ba = ms.ToArray();
            // Console.WriteLine("Send Start");
            session.Send(ba, 0, ba.Length);
        }
    }


    /// <summary>
    /// Process a ChannelDescribe message and send out a ChannelMetadata message 
    /// </summary>
    /// <param name="session">the session that sent the message</param>
    /// <param name="cd">the ChannelDescribe message</param>
    private void ProcessChannelDescribe(WebSocketSession session, ChannelDescribe cd)
    {
        Console.WriteLine("ChannelDescribe:");
        foreach (var u in cd.Uris)
        {
            Console.WriteLine("URI: " + u.ToString());
        }
        if (_metadata != null)
        {
            SendMetadata(session);
        }
    }
    /// <summary>
    /// Process ChannelMetadata message
    /// If consumer sessions are attached, send them the ChannelMetadata
    /// Send a ChannelStreamingStart to the session that sent the ChannelMetadata
    /// </summary>
    /// <param name="session">the session that sent the metadata</param>
    /// <param name="cm">ChannelMetadata message</param>
    private void ProcessChannelMetadata(WebSocketSession session, ChannelMetadata cm)
    {
        _metadata = cm;
        // If we have connected consumers, send the metadata to them
        foreach (var s in session.AppServer.GetAllSessions())
        {
            Console.WriteLine("Check session: " + s.SessionID);
            ETPSession es = _sessions[s.SessionID];
            if (es != null)
            {
                if (es.role.Equals("consumer") && 
                    !session.SessionID.Equals(s.SessionID)) // Send to consumers and not to this current session
                {
                    Console.WriteLine("Send ChannelMetadata to: " + es.appName, " (" + es.id+ ")");
                    SendMetadata(s);
                }
            }
        }
        SendChannelStreamingStart(session); // Signal the sender of the Metadata to start streaming data
    }
    /// <summary>
    /// Loop through the attached sessions and send data to any consumers that are not currently interrupted
    /// </summary>
    /// <param name="session">the session that sent the ChannelData</param>
    /// <param name="cd">the ChannelData message</param>
    private void ProcessChannelData(WebSocketSession session, ChannelData cd)
    {
        foreach (var s in session.AppServer.GetAllSessions())
        {
            ETPSession es = _sessions[s.SessionID];
            if (es.role.Equals("consumer") && !es.interrupted && 
                !session.SessionID.Equals(s.SessionID)) // Send to consumers and not to this current session
            {
                Console.WriteLine("Send ChannelData to: " + es.appName + " (" + es.id+")");
                SendChannelData(s, cd);
            }
        }

    }
    /// <summary>
    /// Send out ChannelMetadata to a session
    /// </summary>
    /// <param name="session">the session to which the ChannelMetadata is sent</param>
    private void SendMetadata(WebSocketSession session)
    {
        // Send a MessageHeader, followed by ChannelMetadata Message
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 1; // Streaming protocol
        mh.MessageType = 2; // ChannelMetadata message
        mh.MessageId = _counter++;
        // Send the global ChannelMetadata message
        // Now write the messages to the WebSocket. 
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<ChannelMetadata>(_metadata.Schema);
            bodyWriter.Write(_metadata, encoder);
            byte[] ba = ms.ToArray();
            session.Send(ba, 0, ba.Length);
        }
    }
    /// <summary>
    /// Send out ChannelData to a session
    /// </summary>
    /// <param name="session">the session to which the ChannelData is sent</param>
    /// <param name="cd">the ChannelData Avro message</param>
    private void SendChannelData(WebSocketSession session, ChannelData cd)
    {
        // Send a MessageHeader, followed by ChannelData Message
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 1; // Streaming protocol
        mh.MessageType = 3; // ChannelData message
        mh.MessageId = _counter++;
        // Send the global ChannelMetadata message
        // Now write the messages to the WebSocket. 
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<ChannelData>(cd.Schema);
            bodyWriter.Write(cd, encoder);
            byte[] ba = ms.ToArray();
            session.Send(ba, 0, ba.Length);
        }
    }
    /// <summary>
    /// Send a ChannelStreamingStart message
    /// </summary>
    /// <param name="session">the session to send it to</param>
    private void SendChannelStreamingStart(WebSocketSession session)
    {
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 1; // Streaming data protocol
        mh.MessageType = 4; // ChannelStreamingStart message to tell producer to start sending data
        mh.MessageId = _counter++;

        // Create a ChannelStreamingStart message
        var cs = new ChannelStreamingStart();
        var l = new List<ChannelStreamingInfo>(); // Empty list of required channels to signal a SimpleStreamer to start
        cs.Channels = l;
        // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<ChannelStreamingStart>(cs.Schema);
            bodyWriter.Write(cs, encoder);
            byte[] ba = ms.ToArray();
            session.Send(ba, 0, ba.Length);
        }
    }

    /// <summary>
    /// This class is used to maintain some basic data for connected ETP applications.
    /// The data are stored in a static global Dictionary (_sessions), keyed on the session identifier
    /// </summary>
    private class ETPSession
    {
        public string id = "";          // WebSocket session id, also used as the key
        public string role = "";        // "producer" or "consumer" from the RequestSession message
        public string appName = "";     // The name of the application from the RequestSession message
        public bool interrupted = true; // For a consumer if it has sent a ChannelStreamingStop
    }

}
