ETP Prototype for C#
--------------------

John Shields - 27 August 2014

The ETP Prototype for C# delivers the following console applications. The source for each is in a single 
C# source code file

ETPServer   - ETP Protocol 1 server (ETPServer.cs)
ETPConsumer - Simple ETP data consumer (ETPConsumer.cs)
ETPProducer - Simple ETP data producer (ETPProducer.cs)
TestUtility - Test routines for the Date/Time utilities (TestUtility.cs)

In the ETPPrototype top level folder there is a Visual Studio solution file (ETPPrototype.sln). 
This was created with Visual Studio 2013. 
The solution uses NuGet to load the ETP schemas and derived C# files along with the required
Avro, NewtonSoft.JSON and log4net DLLs. Right-click on the Solution in the Solution Explorere and 
select "Manage NuGet Packages for Solution".

This is the folder layout under the top level ETPPrototype folder:

.nuget      - Utilities used by NuGet
Common      - Contains ETPUtil.cs with utility methods shared by the applications
ETPConsumer - Simple data consumer in ETPConsumer.cs
ETPProducer - Simple data producer in ETPProducer.cs
ETPServer   - Basic ETP server in ETPServer.cs to demonstrate WebSocket and Avro processing
Libraries   - Copies of the 3rd party libraries requires (WebSocket4Net and SuperWebSocket)
Output      - The location for all compiled output binaries and libraries
packages    - The packages downloaded via NuGet (Avro, NewtonSoft, log4net and ETP)
TestUtility - Contains TestUtility.cs to test the Date/Time utility methods.

ETPServer
---------
Demonstrates how to set up a WebSocketServer to receive and process ETP messages for protocols 0 and 1.
Only supports a single data producer sending data to the server.
Supports multiple consumers receiving data from the server.
Does not support channel selection. Just sends all data to consumers.
Responds to ChannelStreamingStop/Start from consumers.
Start this application before starting ETPProducer or ETPConsumer.
Press any key to exit the console application.

ETPProducer
-----------
Gets data from a WITSML log file and sends it once a second as ETP messages to an ETP server.
Sends the data as time-indexed data at the current date/time.
Loops around at the end of the input data file.
Press any key to exit the console application.

ETPConsumer
-----------
Attaches to server and sends a ChannelDescribe with a URI of "*" to request all data.
Press 'S' or 's' to stop/start data transfer from the server. This sends ChannelStreamingStart
and ChannelStreamingStop messages to the server.
Press 'X' or 'x' to exit.

ETPUtil.cs
----------
This module is in the "Common" folder and is shared by the applications.
It contains some utility methods for converting betweeen W3C date/time strings 
and ETP DateTime objects

Issues
------
When the ETPConsumer is interrupted, it can cause an error on the WebSocket that also 
causes ETPProducer to be interrupted. ETPServer remains running though.
