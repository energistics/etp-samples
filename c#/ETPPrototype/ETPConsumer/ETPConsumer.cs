﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using WebSocket4Net;
//
// These are from the Apache Avro DLL
//
using Avro;
using Avro.IO;
using Avro.File;
using Avro.Generic;
using Avro.Specific;
//
// These are from ETP.dll containing classes derived from the ETP JSON schemas
//
using Energistics.Datatypes;
using Energistics.Datatypes.ChannelData;
using Energistics.Protocol.Core;
using Energistics.Protocol.ChannelStreaming;

//
// Simple Application to connect to a WebSocket server as a data consumer and exchange ETP Avro messages
//
// The sequence is:
// This Application                                     WebSocketServer
// ----------------                                     ---------------
// Connect to WebSocket Server     
// Send RequestSession for Protocol 1             
//                                                      Send OpenSession
// Send Start Message
// Send ChannelDescribe
//                                                      Send ChannelMetadata
// Send ChannelStreamingStart  
//                                                      Send ChannelData
//
class ETPConsumer
{
    private static WebSocket _websocket = null;
    private static long _counter = 0; // Message counter
    private ChannelMetadata _metadata = null; // For this simple demo, just assume one metadata record
    private int _status = 0; // 0:not running, 1:Running 2:interrupted

    static void Main(string[] args)
    {
        var etp = new ETPConsumer();
        etp.connect();
        while (true)
        {
            Console.WriteLine("Press 'S' key to start/stop transmission, 'X' to exit...");
            var k = Console.ReadKey();
            if (k.KeyChar == 'S' || k.KeyChar == 's')
            {
                if (etp.GetStatus() == 1)
                {
                    etp.SendStopStreaming();
                }
                else
                {
                    etp.SendStartStreaming();
                }
            }
            if (k.KeyChar == 'X' || k.KeyChar == 'x') break;
        }
        SendCloseSession();
        _websocket.Close(@"ETPConsumer Terminated");
    }
    //
    // Connect to the WebSocket server and set up the callback methods
    //
    private void connect()
    {
        var l = new List<KeyValuePair<string, string>>();
        l.Add(new KeyValuePair<string,string>("etp-encoding","binary"));    // Set up the additional ETP parameter(s)
        _websocket = new WebSocket("ws://localhost:8080", "", null, l); // Configure the connection to the WebSocket server
        _websocket.Opened += new EventHandler(websocket_Opened);
        _websocket.Error += new EventHandler<SuperSocket.ClientEngine.ErrorEventArgs>(websocket_Error);
        _websocket.Closed += new EventHandler(websocket_Closed);
        _websocket.MessageReceived += new EventHandler<MessageReceivedEventArgs>(websocket_MessageReceived);
        _websocket.DataReceived += new EventHandler<DataReceivedEventArgs>(websocket_DataReceived);
        _websocket.Open();
    }

    public int GetStatus()
    {
        return (_status);
    }
    
    //  
    //
    // When the WebSocket is opened, send a RequestSession message
    //
    private void websocket_Opened(object sender, EventArgs e)
    {
        Console.WriteLine("WebSocket Opened");
        _status = 1;
        // Create an ETP MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 0;
        mh.MessageType = 1; // RequestSession message type
        mh.MessageId = _counter++;

        // Create a list (of 1) SupportedProtocol object for a consumer of protocol 1 data
        var sp = new SupportedProtocol();
        sp.Protocol = 1;
        sp.Role = @"consumer";
        var dict = new Dictionary<string, DataValue>(); // This is for a list of protocol Capabilities, if required. In this case it is empty
        sp.ProtocolCapabilities = dict;
        var v = new Energistics.Datatypes.Version();
        v.Major = 1;
        v.Minor = 0;
        v.Revision = 0;
        v.Patch = 0;
        sp.ProtocolVersion = v;
        var lsp = new List<SupportedProtocol>(); // Create a list of supported protocol objects to populate the RequestSession message
        lsp.Add(sp);
        // Create an ETP RequestSession message
        var rs = new RequestSession();
        rs.ApplicationName = @"ETPConsumer"; // The name of this application
        rs.RequestedProtocols = lsp; // Add list of requested protocols

        // Now write the MessageHeader and the RequestSession messages to the WebSocket
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<RequestSession>(rs.Schema);
            bodyWriter.Write(rs, encoder);
            byte[] ba = ms.ToArray();
            _websocket.Send(ba,0,ba.Length);
        }
        // This will trigger the server to send an OpenSession message which will be received via the websocket_DataReceived() method below
    }
    //
    // This is called when the WebSocket is closed
    //
    private void websocket_Closed(object sender, EventArgs e)
    {
        Console.WriteLine("WebSocket Closed");
    }
    //
    // This is for text messages received
    //
    private void websocket_MessageReceived(object sender, EventArgs e)
    {
        Console.WriteLine("Text Message Received: "+e.ToString());
    }
    //
    // This is where binary messages are received
    //
    void websocket_DataReceived(object sender, DataReceivedEventArgs e)
    {
        Console.WriteLine("Message received: length: " + e.Data.Length);
        var mlist = new List<MemoryStream>();
        mlist.Add(new MemoryStream(e.Data));
        var bis = new ByteBufferInputStream(mlist);
        var decoder = new BinaryDecoder(bis); // This can be re-used 
        var mh = new MessageHeader();
        var headerReader = new SpecificReader<MessageHeader>(mh.Schema,mh.Schema);
        headerReader.Read(mh, decoder);
        Console.WriteLine("Header Message: Protocol: " + mh.Protocol + " Message Type: " + mh.MessageType + " Message Id: " + mh.MessageId);
        switch (mh.Protocol)
        {
            case 0: // Core Protocol Messages
                switch (mh.MessageType)
                {
                    case 1: // Request session
                        break;
                    case 2: // Open Session
                        var os = new OpenSession();
                        var osReader = new SpecificReader<OpenSession>(os.Schema,os.Schema);
                        osReader.Read(os, decoder);
                        ProcessOpenSession(os);
                        break;
				    case 5: // Close the session. Need to decode the CloseSession message here
					    Console.WriteLine("ETP close session ");
					    break;
                }
                break;
            case 1: // Streaming protocol messages
                switch (mh.MessageType)
                {
				    case 1: // Channel Describe message - should not be received by a consumer. This message is sent from a consumer to a producer. 
					    break;
				    case 2: // Channel meta data
                        var cmd = new ChannelMetadata();
                        var cmdReader = new SpecificReader<ChannelMetadata>(cmd.Schema, cmd.Schema);
                        cmdReader.Read(cmd, decoder);
                        ProcessChannelMetadata(cmd);
					    break;
				    case 3: // Channel data
                        var cd = new ChannelData();
                        var cdReader = new SpecificReader<ChannelData>(cd.Schema, cd.Schema);
                        cdReader.Read(cd, decoder);
                        ProcessChannelData(cd);
					    break;
                }
                break;
        }
    }
    private void websocket_Error(object sender, EventArgs e)
    {
        Console.WriteLine("Error: " + e.ToString());
    }
    //
    // Process an OpenSession message and send out a Describe message to request metadata from the producer
    //
    private void ProcessOpenSession(OpenSession os)
    {
        Console.WriteLine("OpenSession: App Name: " + os.ApplicationName);
        bool isProducer = false; // Check if the server supports protocol 1 as a producer
        foreach (var p in os.SupportedProtocols)
        {
            Console.WriteLine("Protocol: " + p.Protocol + " Role: " + p.Role);
            if (p.Protocol == 1 && p.Role.Equals("producer"))
            {
                isProducer = true;
                break;
            }
        }
        if (isProducer)
        {
            // Send a MessageHeader, followed by a Start Message
            // Create a MessageHeader message
            var mh = new MessageHeader();
            mh.Protocol = 1; // Streaming data protocol
            mh.MessageType = 0; // Start message to specify the required data
            mh.MessageId = _counter++;
            // Create a Start message
            var st = new Start();
            st.MaxDataItems = 0; // Not sure what to put in here
            st.MaxMessageRate = 0; // Not sure what to put in here
            // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
            using (var ms = new MemoryStream())
            {
                var encoder = new BinaryEncoder(ms);
                var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
                headerWriter.Write(mh, encoder);
                var bodyWriter = new SpecificWriter<Start>(st.Schema);
                bodyWriter.Write(st, encoder);
                byte[] ba = ms.ToArray();
                _websocket.Send(ba, 0, ba.Length);
            }


            // Send a MessageHeader, followed by a ChannelDescribe message
            // Create a 
            mh = new MessageHeader();
            mh.Protocol = 1; // Streaming data protocol
            mh.MessageType = 1; // Describe message to specify the required data
            mh.MessageId = _counter++;

            // Create a ChannelDescribe message
            var cd = new ChannelDescribe();
            string[] stra = { "*" }; // Bogus URI to request everything from the server
            cd.Uris = stra;
            // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
            using (var ms = new MemoryStream())
            {
                var encoder = new BinaryEncoder(ms);
                var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
                headerWriter.Write(mh, encoder);
                var bodyWriter = new SpecificWriter<ChannelDescribe>(cd.Schema);
                bodyWriter.Write(cd, encoder);
                byte[] ba = ms.ToArray();
                _websocket.Send(ba, 0, ba.Length);
            }
        }
    }
    //
    // Process a ChannelMetadata message and send out a ChannelStreamingStart message to request data
    //
    private void ProcessChannelMetadata(ChannelMetadata cmd)
    {
        _metadata = cmd; // Cache the metadata in a global variable
        Console.WriteLine("ChannelMetadata");
        foreach (var c in cmd.Channels)
        {
            Console.WriteLine("Channel: " + c.ChannelUri + " Id: "+c.ChannelId);
        }
        SendStartStreaming();
    }

    private void SendStartStreaming()
    {
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 1; // Streaming data protocol
        mh.MessageType = 4; // ChannelStreamingStart message to tell producer to start sending data
        mh.MessageId = _counter++;

        // Create a ChannelStreamingStart message
        var cs = new ChannelStreamingStart();
        var l = new List<ChannelStreamingInfo>(); // Empty list of required channels to signal a SimpleStreamer to start
        cs.Channels = l;
        // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<ChannelStreamingStart>(cs.Schema);
            bodyWriter.Write(cs, encoder);
            byte[] ba = ms.ToArray();
            _websocket.Send(ba, 0, ba.Length);
        }
        _status = 1; // Started
    }

    private void SendStopStreaming()
    {
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 1; // Streaming data protocol
        mh.MessageType = 5; // ChannelStreamingStop message to tell producer to stop sending data
        mh.MessageId = _counter++;

        // Create a ChannelStreamingStop message
        var cs = new ChannelStreamingStop();
        int[] arr = { }; ; // Empty list of required channel indexes to signal a SimpleStreamer to stop
        cs.Channels = arr;
        // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<ChannelStreamingStop>(cs.Schema);
            bodyWriter.Write(cs, encoder);
            byte[] ba = ms.ToArray();
            _websocket.Send(ba, 0, ba.Length);
        }
        _status = 2; // Stopped
    }
    /// <summary>
    /// Close the WebSocket session
    /// </summary>
    private static void SendCloseSession()
    {
        // Create a MessageHeader message
        var mh = new MessageHeader();
        mh.Protocol = 0;    // Core protocol
        mh.MessageType = 5; // CloseSession message
        mh.MessageId = _counter++;

        // Create a ChannelStreamingStop message
        var cs = new CloseSession();
        cs.Reason = @"Application ETPConsumer terminated";
        // Now write the messages to the WebSocket. It should be possible to make this code more generic and re-usable
        using (var ms = new MemoryStream())
        {
            var encoder = new BinaryEncoder(ms);
            var headerWriter = new SpecificWriter<MessageHeader>(mh.Schema);
            headerWriter.Write(mh, encoder);
            var bodyWriter = new SpecificWriter<CloseSession>(cs.Schema);
            bodyWriter.Write(cs, encoder);
            byte[] ba = ms.ToArray();
            _websocket.Send(ba, 0, ba.Length);
        }
    }


    //
    // Process a ChannelData message
    //
    private void ProcessChannelData(ChannelData cd)
    {
        Console.WriteLine("ChannelData");
        foreach (var c in cd.Data)
        {
            string index;
            // This assumes that the Channel id values data are in an array sequence, starting at 0. 
            // Otherwise we would need a more sophisticated look-up into the metadata array
            if (_metadata.Channels[c.ChannelId].Indexes[0].IndexType.Equals(ChannelIndexTypes.Time))
            {
                Energistics.Datatypes.DateTime dt = (Energistics.Datatypes.DateTime)c.Indexes[0].Item;
                index = ETPUtil.EtpToW3CTime(dt);
            }
            else
            {
                double d = (double)c.Indexes[0].Item;
                index = d.ToString(); // for a "double" or "long" index data type
            }
            if (_metadata.Channels[c.ChannelId].DataType.Equals("date time"))
            {
                var time = (Energistics.Datatypes.DateTime)c.Value.Item; // ETP DateTime object
                string st = ETPUtil.EtpToW3CTime(time);
                Console.WriteLine("ChannelId: " + c.ChannelId + " Index: " + index + " Value: " + st);
            }
            else
            {
                Console.WriteLine("ChannelId: " + c.ChannelId + " Index: " + index + " Value: " + c.Value.Item);
            }
            
        }
    }
}
