﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Avro;

/// <summary>
/// Test routine for the Date/Time utilities
/// </summary>
class TestUtility
{
    static void Main(string[] args)
    {
        show(@"2014-08-22T12:38:45Z");
        show(@"2014-08-22T12:45:45.5+01:30");
    }

    static void show(string dt)
    {
        Console.WriteLine("Original: " + dt);
        Energistics.Datatypes.DateTime edt = ETPUtil.ParseW3CDatetoEtp(dt);
        Console.WriteLine("ETP Time: " + edt.Time + " offset: " + edt.Offset);
        Console.WriteLine("Convert to W3C: " + ETPUtil.EtpToW3CTime(edt));
        System.DateTime sdt = ETPUtil.ParseW3CDate(dt);
        Console.WriteLine("System DateTime: " + sdt.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
        Console.WriteLine("---------------------------------------------------");
    }
}

