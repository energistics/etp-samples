﻿using System;

///
///
/// Parse a date in one of these formats: 
///	2010-12-01
///	2010-12-01T18:05:08Z
///	2010-12-01T18:05:08.123-06:00
/// 
/// Returns a DateTime object
/// 
public class ETPUtil
{
	public static Energistics.Datatypes.DateTime ParseW3CDatetoEtp(string s)
	{
		int year=0;
		int month=0;
		int day=0;
		int hour=0;
		int minute=0;
		int second=0;
		int milli=0;
		long houroffset=0;
		long minuteoffset=0;
		int len = s.Length;
		if(len >=4) year = int.Parse(s.Substring(0,4));
        if (len >= 7) month = int.Parse(s.Substring(5, 2));
        if (len >= 10) day = int.Parse(s.Substring(8, 2));
        if (len >= 13) hour = int.Parse(s.Substring(11, 2));
        if (len >= 16) minute = int.Parse(s.Substring(14, 2));
        if (len >= 19) second = int.Parse(s.Substring(17, 2));
		int pos = len;
		int mend = len;
		int offsetFactor = 0; // =1 for positive time offset, -1 for negative time offset
		// Console.WriteLine("Y:"+year+" M:"+month+" D:"+day+" h:"+hour+" m:"+minute+" s:"+second);
		int zpos = s.IndexOf('Z');
		int ppos = s.IndexOf('+');
		int mpos = s.LastIndexOf('-');
		if (zpos > 0)
        {
            mend = zpos;
        }
		if (ppos > 18)
		{
			mend = ppos;
			offsetFactor = 1;
		}
		if (mpos > 18)
		{
			mend = mpos;
			offsetFactor = -1;
		}
        //  Console.WriteLine("len: " + len + " mend: " + mend + " ppos: " + ppos + " mpos: " + mpos);
		if (len > 21 && s[19] == '.')
		{
			milli = (int)(1000 * Double.Parse(s.Substring(19,mend-19)));  // parse the string beginning with the decimal point
            // Console.WriteLine("Millis: "+milli);
		}
		// Handle time offset +/-hh:mm
		if (len > mend+3 && offsetFactor != 0)
		{
            houroffset = Int64.Parse(s.Substring(mend + 1, 2));
            minuteoffset = Int64.Parse(s.Substring(mend + 4));
            // Console.WriteLine("Hour offset: " + houroffset + " Minute offset: " + minuteoffset);
		}

        var utcTime = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc);
        utcTime = utcTime.AddMilliseconds(milli);
        // Console.WriteLine("Time (no offset): "+utcTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"));
        var udt = new System.DateTime(1970, 1, 1, 0, 0, 0, 0); 
        float offset = offsetFactor * (houroffset + minuteoffset/60f); // Offset in hours
        // Console.WriteLine("ETP Hour offset: " + offset);
        utcTime = utcTime.AddHours(-1.0 * offset); // Convert the W3C time to UTC
        var dt = new Energistics.Datatypes.DateTime();
        dt.Time = (utcTime.Ticks - udt.Ticks)/ 10; // Convert ticks to microseconds 1 tick is 100 nanoseconds, 10 ticks is a microsecond
        dt.Offset = offset; // Offset in hours
        // Console.WriteLine("ETPTime time: " + dt.Time + " offset: " + dt.Offset);
		return(dt);
	}
    /// <summary>
    /// Parse a W3C date/time string into a System.DateTime object
    /// 
    /// Maybe need to add another method to return a DateTimeOffset object to preserve the original time and offset??
    /// </summary>
    /// <param name="s">W3C Date/Time string</param>
    /// <returns>.NET date/time object</returns>
    public static DateTime ParseW3CDate(string s)
    {
        Energistics.Datatypes.DateTime edt = ParseW3CDatetoEtp(s);
        var udt = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc); 
        var dt = new DateTime(udt.Ticks + edt.Time * 10 + (long)(edt.Offset * 3600 * 1000 * 10000)); // edtTime is in microseconds, edt.Offset is in hours
        return (dt);
    }


    /// <summary>
    /// Convert an ETP DateTime object to a WITSML/W3C/ISO8601 time string
    /// 
    /// TODO: Need to handle timezone offset
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string EtpToW3CTime(Energistics.Datatypes.DateTime dt)
    {
        var udt = new System.DateTime(1970, 1, 1, 0, 0, 0, 0); 
        double dts = ((double)dt.Time) / 1000000 + dt.Offset * 3600.0; // microseconds to decimal seconds + the offset in hours
        var sdt = udt.AddSeconds(dts);
        string sti = sdt.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
        // Console.WriteLine("Base time: " + sti);
        if (dt.Offset != 0.0f) // Offset is in +/- hours
        {
            char sign = (dt.Offset < 0) ? '-' : '+';
            sti = sti.Replace('Z', sign);
            int hours = (int)dt.Offset;
            int minutes = (int)((dt.Offset - hours)*60);
            sti = sti + ETPUtil.PadZero(hours,2) + ":" + ETPUtil.PadZero(minutes,2);
        }
        return (sti);
    }
    /// <summary>
    /// 
    /// Add leading zero characters to the front of a string
    /// PadZero("3",2) returns "03";
    /// </summary>
    /// <param name="s">original string</param>
    /// <param name="length">required length</param>
    /// <returns>padded string</returns>
    public static string PadZero(int num, int length)
    {
	    var st = ""+num;
	    while (st.Length < length)
	    {
		    st = "0" + st;
	    }
	    return(st);
    }

}