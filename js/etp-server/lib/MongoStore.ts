/// <reference path="reference/mongodb.d.ts"/>
/// <reference path="reference/etp.d.ts"/>

import energistics = require("./Energistics");
import ralf = require("etp");
var messages = ralf.Messages.Energistics.Protocol;
import events = require("events");
import mongo = require("mongodb");
import url = require('url');

var WITSML_ROOT:string = "eml://witsml141";

// this is a basic projection for the mongodb aggregation pipeline
// that will convert a document from any collection to an ETP Resource
// struct.
class SimpleProjection  {
    _id: number = 0;
    uri : any = { "$concat": [ WITSML_ROOT, "/", "$_id"] };
    name: string = "$name";
    subscribable: boolean = true;
    customData: any = {};
    resourceType: any =  { $concat: ['DataObject'] };
    hasChildren: number = -1;
}


class ChannelProjection {
    uri:any = {$concat: [WITSML_ROOT, "/", "$_id", "/", "$logCurveInfo.mnemonic"]};
    name:string = "$logCurveInfo.mnemonic";
    subscribable:boolean = true;
    customData:any = {};
    resourceType:any = {$concat: ['DataObject']};
    hasChildren:number = -1;
}

function nonEmptyString(s) {
    return s.length > 0;
}

class EtpUri {
    constructor(private _uri: string) {
        this._url = url.parse(_uri);
        if (this._url.path) {
            this._pathParts = this._url.path.split('/').filter(nonEmptyString);
        }
        for(var i:number = 0; i<this._pathParts.length; i++){
            var attribute = this._pathParts[i].split(/[\(\)]/).filter(nonEmptyString);
            this[attribute[0]]=attribute[1];
        }
    }
    private _url: url.Url;
    private _pathParts: string[]=[];
    type: string;
    id: string;
    isWitsml() : boolean {
        return this._url.hostname=="witsml141";
    }

    get isUrn(): boolean {
        return this._url.protocol == "urn:";
    }

    get isUuid(): boolean {
        return this.isUrn && this._url.hostname == "uuid";
    }

    get isRoot(): boolean {
        return this._uri == "/";
    }
}



class MyResource extends ralf.Messages.Energistics.Datatypes.Object.Resource{
    constructor(public uri: string, public name: string, public resourceType = "DataObject")
    {
        super();
        this.subscribable = true;
        this.hasChildren = -1;
        this.customData = {};
    }
}

class MongoStore extends events.EventEmitter implements energistics.IStore {
    wells = null;
    wellbores = null;
    logs =  null;
    logrecords = null;
    trajectorys = null;
    channels = null;
    db = null;

    constructor(connectString: string) {
        super();
        console.log("Connecting to database: " + connectString);
        mongo.connect(connectString, this.on_connect.bind(this));
    }

    connect(connectString: string) {
    }

    enum(uriString: string, callback:(err, data) => void) {

        var uri = new EtpUri(uriString);
        console.log(uri);

        if(uri.isRoot) {
            callback(null, [new MyResource(WITSML_ROOT,  'WITSML Store', "UriProtocol") /*, new MyResource( 'eml://resqml20',  'RESQML Earth Model', "UriProtocol")*/]);
        }
        else if(uri.isWitsml()) {
            if (uriString == WITSML_ROOT) {
                //var wuri = new EtpUri(uri);
                callback(null, [new MyResource(WITSML_ROOT + '/well', 'Wells', "Folder")])
            }
            else if (uriString == WITSML_ROOT + "/well") {
                this.wells.aggregate([
                    { $project: new SimpleProjection()}
                ]).toArray(callback);
                //this.wells.find().toArray(callback);
            }
            else if (uriString.match(/eml:.*well\([a-z0-9-]+\)$/i)) {
                var wellId = uriString.split("/").pop();
                console.log("Well:" + wellId);
                this.wellbores.aggregate([
                    { $match: { ParentId: wellId } },
                    { $project: new SimpleProjection()}
                ]).toArray(callback);
            }
            else if (uriString.match(/eml:.*wellbore\([a-z0-9-]+\)$/i)) {
                var wellboreId = uriString.split("/").pop();
                console.log("Wellbore:" + wellboreId);
                this.logs.aggregate([
                    { $match: { ParentId: wellboreId } },
                    { $project: new SimpleProjection()}
                ]).toArray(callback);
                //this.logs.aggregate({ $match: { ParentId: uri } , $project: SimpleProjection}).toArray(callback);
            }
            else if (uriString.match(/eml:.*log\([a-z0-9-]+\)$/i)) {
                var logId = uriString.split("/").pop();
                console.log("Log:" + logId);
                this.logs.aggregate([
                    { $match: { _id: logId } },
                    { $unwind: "$logCurveInfo"},
                    { $project: new ChannelProjection()}
                ]).toArray(callback);
            }
        }
    }

    get(uriString: string, callback:(err, data) => void) {
        var uri = new EtpUri(uriString);
        console.log(uri);

        if(uri.isWitsml()) {
            if (uriString == WITSML_ROOT) {
            }
            else if (uriString == WITSML_ROOT + "/well") {
            }
            else if (uriString.match(/eml:.*well\([a-z0-9-]+\)$/i)) {
            }
            else if (uriString.match(/eml:.*wellbore\([a-z0-9-]+\)$/i)) {
            }
            else if (uriString.match(/eml:.*log\([a-z0-9-]+\)$/i)) {
                var logId = uriString.split("/").pop();
                console.log("Log:" + logId);
                this.logs.aggregate([
                    { $match: { _id: logId } },
                ]).toArray(callback);
            }
        }
    }

    getHistory(uri: string, callback:(err, data) => void) {
        return this.logrecords.find({ParentId: uri}).sort({_id: 1});
    }

    put(obj: string)
    {

    }

    putChannelValue(uriChannel, index, value)
    {

    }

    delete(uri:string)
    {

    }

    on_connect(err, db) {
        if(err) { console.dir(err); }
        this.db=db;
        this.wells = db.collection('wells');
        this.wellbores = db.collection('wellbores');
        this.logs = db.collection('logs');
        this.trajectorys = db.collection('logs');
        this.logrecords = db.collection('logrecords');
        this.channels = db.collection('channels');
        this.emit('connect');
    }
}

export = MongoStore;