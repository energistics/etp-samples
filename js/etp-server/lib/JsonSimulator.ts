///<reference path='reference/node.d.ts'/>
import events = require("events");

export class JsonSimulator extends events.EventEmitter {

    rowsPerBlock: number = 1;
    channels: number = 0;
    timer = null;
    log = { data: [], logCurveInfo: [] };
    uri: string = "";
    rowid: number = 0;
    running: boolean = false;
    channelMetadata = [];
    speedUp: number = 10;

    dataBlock() {
        var channelIds = new Array(this.channels);
        for (var i = 0; i < this.channels; i++)
            channelIds[i] = i + 1;
        if (this.log && this.log.data && (this.log.data.length > 0) && this.running) {
            // Take a slice of a given size, and increment the current position at the same time.
            var data = this.log.data.slice(this.rowid, (this.rowid += this.rowsPerBlock));
            if (data.length > 0) {
                var rowData = new Array(data.length);
                for (var row = 0; row < data.length; ++row) {
                    var parts = data[row].split(",");
                    var indexValue = { item: { DateTime: { time: new Date().getTime(), offset: 0}} };
                    var channelData = new Array(this.channels);
                    for (var channel = 0; channel < this.channels; channel++) {
                        channelData[channel] = { item: { string: parts[channel]} };
                    }
                    rowData[row] = { index: indexValue, data: channelData };
                }
                this.emit("data", { channels: channelIds, data: rowData });
            }
            else {
                console.log("No more data: " + this.uri);
                if (this.timer !== null)
                    clearInterval(this.timer);
                this.timer = null;
                this.emit("endofdata", this.uri);
            }
        }
    }

    nextRow() {
        if (this.running && this.log && this.log.data && this.log.data.length > 0) {
            var thisRow = this.log.data.shift();
            var rowData = new Array(this.channels);
            var parts = thisRow.split(",");
            var indexValue = { item: { 'Energistics.Datatypes.DateTime': { time: new Date(parts[0]), offset: 0}} };
            var channelData = new Array(this.channels);
            rowData[0] = { index: {'Energistics.Protocol.ChannelData.RowIndex': indexValue}, channelId: 0, value: { string: parts[0] } };
            for (var channel = 1; channel < this.channels; channel++) {
                rowData[channel] = { channelId:this.channelMetadata[channel].channelID, value: { item: { string: parts[channel]} } };
            }
            var nextRow = this.log.data[0];
            this.emit("data", { data: rowData });
            if (nextRow !== undefined && this.running) {
                var thisTime = new Date(parts[0]);
                var nextTime = new Date(nextRow.split(",")[0]);
                var waitTime = (nextTime.valueOf() - thisTime.valueOf());
                setTimeout(this.nextRow.bind(this), waitTime / this.speedUp);
            }
        }
        else {
            // Wait to see if data shows up.
            setTimeout(this.nextRow.bind(this), 1000);
        }
    }

    simulateRealTime() {
        this.running = true;
        this.nextRow();
    }

    bulkTransfer(caps) {
        this.running = true;
        this.rowsPerBlock = caps.maxDataBlockRows;
        if (caps.maxDataBlockRate >= 0) {
            this.timer = setInterval(this.dataBlock.bind(this), caps.maxDataBlockRate);
        }
        else {
            // We are using an explicit acknowledgement
            this.dataBlock.bind(this);
        }
    }

    stop() {
        this.running = false;
    }

    describe(uris, newChannelId) {
        this.uri = uris[0];
        var fileName = "../logs/" + this.uri.replace(/\//g, "_");
        this.log = require(fileName + ".json");
        this.channels = this.log.logCurveInfo.length;
        //console.dir(this.log);
        this.channelMetadata = new Array(this.channels);
        for (var i = 0; i < this.channelMetadata.length; i++) {
            var info = this.log.logCurveInfo[i];
            this.channelMetadata[i] = {
                indexId: 0,
                channelUri: this.uri + "/logCurve(" + info.mnemonic + ")",
                channelID: newChannelId(),
                dataType: "float",
                uom: info.unit,
                startIndex: null,
                endIndex: null,
                mnemonic: info.mnemonic
            };

        }
        // load the log data.
        this.log.data = require(fileName + "_data.json");

        this.emit('metadata', { indexes: [
            { indexType: "Time", indexUri: this.uri + "/logCurve(Time)", indexID: 0, uom: "ms", datum: null }
        ], channels: this.channelMetadata });
    }
}