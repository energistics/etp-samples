///<reference path='reference/node.d.ts'/>
import events = require("events");

class StoreSimulator extends events.EventEmitter {

    rowsPerBlock: number = 1;
    channels: number = 0;
    timer = null;
    log = { data: [], logCurveInfo: [], indexCurve: { columnIndex: 1 } };
    uri: string = "";
    rowid: number = 0;
    running: boolean = false;
    channelMetadata = [];
    rowBuffer = [];
    speedUp: number = 10;
    newChannelId = null;
    store = null;
    cursor = null;
    currentRow = null;
    realTime = true;

    dataBlock(data) {
        if (this.running && data) {
            var channelIds = new Array(this.channels);
            for (var i = 0; i < this.channels; i++)
                channelIds[i] = this.channelMetadata[i].channelID;

            var indexIndex = this.log.indexCurve.columnIndex - 1;
            if (data.length > 0) {
                var rowData = new Array(data.length);
                for (var row = 0; row < data.length; ++row) {
                    var parts = data[row];
                    var indexValue = { item: { 'Energistics.Datatypes.DateTime': { time: new Date(parts[indexIndex]), offset: 0}} };
                    var channelData = new Array(this.channels);
                    for (var channel = 0; channel < this.channels; channel++) {
                        channelData[channel] = { item: { string: parts[channel]} };
                    }
                    rowData[row] = { index: indexValue, data: channelData };
                }
                this.emit("data", { channels: channelIds, data: rowData });
            }
            else {
                console.log("No more data: " + this.uri);
                if (this.timer !== null)
                    clearInterval(this.timer);
                this.timer = null;
                this.emit("endofdata", this.uri);
            }
        }
    }

    nextRow(data: any) {
        if (this.running && this.currentRow) {
            var indexIndex = this.log.indexCurve.columnIndex - 1;
            var parts = this.currentRow;
            var rowData = new Array(this.channels);
            var indexValue = { item: { 'Energistics.Datatypes.DateTime': { time: new Date(parts[indexIndex]), offset: 0}} };
            var channelData = new Array(this.channels);
            rowData[0] = { index: {'Energistics.Protocol.ChannelData.RowIndex': indexValue}, channelId: this.channelMetadata[indexIndex].channelID, value: { item: { string: parts[0] }}  };
            for (var channel = 1; channel < this.channels; channel++) {
                rowData[channel] = { channelId:this.channelMetadata[channel].channelID, value: { item: { string: parts[channel]} } };
            }
            this.emit("data", { data: rowData });
            if (this.running) {
                var nextTime = new Date(data[0]);
                var thisTime = new Date(this.currentRow[indexIndex]);
                var waitTime = (nextTime.valueOf() - thisTime.valueOf());
                setTimeout(function(){
                    this.cursor.nextObject(this.nextObject.bind(this));
                }.bind(this), waitTime / this.speedUp);
                this.currentRow = data;
            }
        }
        else {
            this.stop();
        }
    }

    nextObject(err, data: any) {
        if(data) {
            this.nextRow(data.Data);
        }
        else {
            throw err;
        }
    }

    startStreaming(channels) {
        this.running = true;
        // Get the first row
        this.cursor.nextObject(function(err, data: any){
            if (data)
                this.currentRow=data.Data;
            else
                throw console.dir(err);
        }.bind(this));
        this.cursor.nextObject(this.nextObject.bind(this));
    }

    stopStreaming(channels) {

    }

    nextBulkRow(data: any) {
        this.rowBuffer.push(data.Data);
        if(this.rowBuffer.length==100) {
            this.dataBlock(this.rowBuffer);
            this.rowBuffer = [];
        }
    }

    bulkTransfer(caps) {
        this.running = true;
        //this.cursor.nextObject(this.nextBulkRow.bind(this));

         var stream  = this.cursor.stream();
         stream
             .on('data', this.nextBulkRow.bind(this))
             .on('error', function (err) { }.bind(this))
             .on('close', function () {
            this.dataBlock(this.rowBuffer);
            this.stop();
         }.bind(this));

        //this.rowsPerBlock = caps.maxDataBlockRows;
        //if (caps.maxDataBlockRate >= 0) {
        //    this.timer = setInterval(this.dataBlock.bind(this), caps.maxDataBlockRate);
        //}
        //else {
            // We are using an explicit acknowledgement
        //    this.dataBlock.bind(this);
        //}
    }

    stop() {
        this.running = false;
    }

    handleMetadata(err, data) {
        this.log = data[0];
        this.channels = this.log.logCurveInfo.length;
        //console.dir(this.log);
        this.channelMetadata = new Array(this.channels);
        for (var i = 0; i < this.channelMetadata.length; i++) {
            var info = this.log.logCurveInfo[i];
            var dataTypeSchema = "double"
            switch (info.typeLogData) {
                case 0:
                    dataTypeSchema = "Energistics.Datatype.DateTime";
                    break;
                case 1:
                    dataTypeSchema = "double";
                    break;
                case 2:
                    dataTypeSchema = "long";
                case 3:
                    dataTypeSchema = "string";
            }
            this.channelMetadata[i] = {
                channelUri: this.uri + "/logCurve(" + info.mnemonic + ")",
                channelId: this.newChannelId(),
                indexes: [
                    { indexType: "Time", uri: {string: this.uri + "/logCurve(Time)"}, direction: "Increasing", mnemonic: {string: "Time"}, indexID: 0, uom: "ms", datum: null }
                ],
                dataType: dataTypeSchema,
                uom: info.unit,
                startIndex: null,
                endIndex: null,
                mnemonic: info.mnemonic,
                description: info.mnemonic,
                status: "Active"
            };
        }

        this.emit('metadata', {  channels: this.channelMetadata });

        this.cursor = this.store.getHistory(this.uri);

        /*
        if(this.realTime)
            this.simulateRealTime();
        else
            this.bulkTransfer({maxDataBlockRows: 100, maxDataBlockRate: 3});
        */
    }

    describe(uris, newChannelId, store) {
        this.uri = uris[0];
        this.store = store;
        this.newChannelId = newChannelId;
        store.get(this.uri, this.handleMetadata.bind(this));
    }
}

export = StoreSimulator;