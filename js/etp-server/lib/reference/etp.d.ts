/// <reference path="node.d.ts" />


declare module "etp" {
    export module Util {
        export class SchemaCache extends avro.SchemaCache {
            constructor();
            find(protocol: number, messageType: number): avro.Schema;
        }
    }

    export module avro {
        /// <reference path="node.d.ts" />
        export  class BinaryReader {
            constructor(schemas: any, buffer: Uint8Array);
            strictMode: boolean;
            buffer: Uint8Array;
            dataView: DataView;
            idx: number;
            schemas: any;
            decode(schema: any, buffer: number[]): any;
            readByte(): number;
            utf8Decode(bytes: any): string;
            typeOf(value: any): string;
            read32le(): number;
            readCount(): number;
            readBoolean(): boolean;
            readInt(): number;
            readLong(): number;
            readFloat(): number;
            readDouble(): number;
            readFixed(len: number): any[];
            readBytes(): any[];
            readString(): string;
            readEnum(): number;
            readArrayStart(): number;
            arrayNext(): number;
            readMapStart(): number;
            mapNext(): number;
            readArray(schema: any): any[];
            readDatum(schema: any): any;
        }
        export  class BinaryWriter {
            constructor(schemas: any);
            buffer: Uint8Array;
            dataView: DataView;
            _index: number;
            schemas: {};
            getBuffer(): Buffer;
            getArrayBuffer(): any;
            alloc(size: number): void;
            realloc(): void;
            require(bytes: any): void;
            encode(schema: any, datum: any): Uint8Array;
            writeByte(b: any): void;
            utf8Encode(value: string): string;
            writeBoolean(value: any): void;
            writeInt(value: any): void;
            writeLong(value: any): void;
            writeFloat(f: any): void;
            writeDouble(value: any): void;
            writeString(str: string): void;
            writeIndex(idx: any): void;
            writeMapStart(): void;
            writeMapEnd(): void;
            writeDatum(schema: any, datum: any): void;
        }
        export interface Schema {
            fullName: string;
            name: string;
            namespace: string;
        }
        export  class SchemaCache {
            constructor(schemaArray: any[]);
            importSchemas(filePath: string): void;
            store(schema: any): void;
            find(protocol: number, messageType: number): Schema;
            validate(schema: string, datum: any);
        }

    }
    export var Schemas:any;

    export module Messages {
        export module Energistics {
            module Datatypes {
                class ArrayOfDouble {
                    public values:number[];
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                class DateTime {
                    public time:number;
                    public offset:number;
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                class DataValue {
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                class DataAttribute {
                    public attributeId:number;
                    public attributeValue:DataValue;
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                enum ErrorCodes {
                    ENOROLE = 0,
                    ENOSUPPORTEDPROTOCOLS = 1,
                    EINVALID_MESSAGETYPE = 2,
                    EUNSUPPORTED_PROTOCOL = 3,
                }
            }
        }
        export module Energistics {
            module Datatypes {
                class MessageHeader {
                    public protocol:number;
                    public messageType:number;
                    public correlationId:number;
                    public messageId:number;
                    public messageFlags:number;
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                enum Protocols {
                    Core = 0,
                    Channel_Data = 1,
                    Channel_Tabular = 2,
                    Discovery = 3,
                    Object_Store = 4,
                    Object_Query = 5,
                }
            }
        }
        export module Energistics {
            module Datatypes {
                class Version {
                    public major:number;
                    public minor:number;
                    public revision:number;
                    public patch:number;
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                class SupportedProtocol {
                    public protocol:number;
                    public protocolVersion:Version;
                    public role:string;
                    public protocolCapabilities:any;
                    public _schema:any;
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    enum ChannelIndexTypes {
                        Time = 0,
                        Depth = 1,
                        ElapsedTime = 2,
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    enum ChannelStatuses {
                        Active = 0,
                        Inactive = 1,
                        Closed = 2,
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    enum ErrorCodes {
                        EINVALID_URI = 0,
                        EINVALID_CHANNELID = 1,
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    enum IndexDirections {
                        Increasing = 0,
                        Decreasing = 1,
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class IndexMetadataRecord {
                        public indexType:ChannelIndexTypes;
                        public uom:string;
                        public direction:IndexDirections;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class IndexValue {
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class ChannelMetadataRecord {
                        public channelUri:string;
                        public channelId:number;
                        public indexes:IndexMetadataRecord[];
                        public mnemonic:string;
                        public dataType:string;
                        public uom:string;
                        public description:string;
                        public status:ChannelStatuses;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class ChannelRangeInfo {
                        public channelId:number;
                        public startIndex:IndexValue;
                        public endIndex:IndexValue;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class DataItem {
                        public indexes:IndexValue[];
                        public channelId:number;
                        public value:DataValue;
                        public valueAttributes:DataAttribute[];
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    enum Roles {
                        Producer = 0,
                        Consumer = 1,
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class StreamingStartIndex {
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelData {
                    class ChannelStreamingInfo {
                        public channelId:number;
                        public startIndex:StreamingStartIndex;
                        public receiveChangeNotification:boolean;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module ChannelDataFrame {
                    class DataRow {
                        public index:ChannelData.IndexValue[];
                        public data:DataValue[];
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module Object {
                    class DataObject {
                        public namespace:string;
                        public version:string;
                        public objectType:string;
                        public contentType:string;
                        public contentEncoding:string;
                        public data:string;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Datatypes {
                module Object {
                    class Resource {
                        public uri:string;
                        public name:string;
                        public subscribable:boolean;
                        public customData:any;
                        public resourceType:string;
                        public hasChildren:number;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Domain {
                module Drilling {
                    class TrajectoryStation {
                        public uuid:string;
                        public _schema:any;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelDataFrame {
                    class ChannelDataFrame {
                        public channels:number[];
                        public data:Datatypes.ChannelDataFrame.DataRow[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelDataFrame {
                    class ChannelMetadata {
                        public indexes:Datatypes.ChannelData.IndexMetadataRecord[];
                        public channels:Datatypes.ChannelData.ChannelMetadataRecord[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelDataFrame {
                    class RequestChannelData {
                        public uri:string;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelData {
                        public data:Datatypes.ChannelData.DataItem[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelDataChange {
                        public channelId:number;
                        public startIndex:Datatypes.ChannelData.IndexValue;
                        public endIndex:Datatypes.ChannelData.IndexValue;
                        public data:Datatypes.ChannelData.DataItem[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelDelete {
                        public channelId:number;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelDescribe {
                        public uris:string[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelMetadata {
                        public channels:Datatypes.ChannelData.ChannelMetadataRecord[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelRangeRequest {
                        public channelRanges:Datatypes.ChannelData.ChannelRangeInfo[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelStreamingStart {
                        public channels:Datatypes.ChannelData.ChannelStreamingInfo[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelStatusChange {
                        public channelId:number;
                        public status:Datatypes.ChannelData.ChannelStatuses;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class ChannelStreamingStop {
                        public channels:number[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module ChannelStreaming {
                    class Start {
                        public maxMessageRate:number;
                        public maxDataItems:number;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Core {
                    class Acknowledge {
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Core {
                    class CloseSession {
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Core {
                    class OpenSession {
                        public applicationName:string;
                        public sessionId:string;
                        public supportedProtocols:Datatypes.SupportedProtocol[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Core {
                    class ProtocolException {
                        public errorCode:number;
                        public errorMessage:string;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Core {
                    class RequestSession {
                        public requestedProtocols:Datatypes.SupportedProtocol[];
                        public applicationName:string;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Discovery {
                    class GetResources {
                        public uri:string;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Discovery {
                    class GetResourcesResponse {
                        public uri:string;
                        public resources:Datatypes.Object.Resource[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Store {
                    class AddToStore {
                        public data:Datatypes.Object.DataObject[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Store {
                    class DeleteFromStore {
                        public uri:string[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Store {
                    class GetFromStore {
                        public uri:string;
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
        export module Energistics {
            module Protocol {
                module Store {
                    class Object {
                        public dataObjects:Datatypes.Object.DataObject[];
                        public _schema:any;
                        static _protocol:number;
                        static _messageTypeId:number;
                    }
                }
            }
        }
    }
}