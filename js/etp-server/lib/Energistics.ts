/// <reference path="JsonSimulator.d.ts"/>
/// <reference path="StoreSimulator.d.ts"/>
/// <reference path="reference/etp.d.ts"/>

import events = require("events");
import etp = require("etp");

var avro = etp.avro;
export var PROTOCOL = etp.Messages.Energistics.Datatypes.Protocols;
var	path = require('path'),
    fs = require('fs'),
    uuid = require('node-uuid'),
    protocol = etp.Schemas;

var appRoot = path.dirname(path.dirname(module.filename));

var NO_SESSION: string = "";

// Define protocol enumerations.

var MSG_START = 0;
var MSG_CORE_REQUESTSESSION = 1;
var MSG_CORE_OPENSESSION = 2;
var MSG_CORE_CLOSESESSION = 5;
var MSG_CORE_EXCEPTION = 1000;
var MSG_CORE_ACKNOWLEDGE = 8;

var MSG_HISTORICAL_REQUEST = 1;
var MSG_HISTORICAL_METADATA = 3;
var MSG_HISTORICAL_DATA = 4;

var MSG_CHANNEL_DESCRIBE = 1;
var MSG_CHANNEL_METADATA = 2;
var MSG_CHANNEL_DATA = 3;
var MSG_CHANNEL_START_STREAMING = 4;
var MSG_CHANNEL_STOP_STREAMING = 5;
var MSG_CHANNEL_DATACHANGE = 6;
var MSG_CHANNEL_DATADELETE = 7;
var MSG_CHANNEL_DELETE = 8;
var MSG_CHANNEL_RANGE_REQUEST = 9;

var MSG_DISCOVERY_GET = 1;
var MSG_DISCOVERY_RESOURCE = 2;

var MSG_STORE_GET = 0;
var MSG_STORE_OBJECT = 3;

var EINVALID_MESSAGE_TYPE = 3;

export interface IStore {
    connect(connectionString: string);
    enum(uri: string, handler:(err, data) => void);
    get(uri:string, handler:(err, data) => void);
    put(obj: string);
    delete(uri:string);
}

export interface ProtocolHandler {
    sessionManager: ETPCore;
    start();
    stop();
    role(): string;
    handleMessage(messageHeader, messageBody);
}

/**
 * 'abstract' Base class for all protocol handlers other than protocol 0
 */
export class BaseProtocolHandler extends events.EventEmitter implements ProtocolHandler {
    sessionManager: ETPCore;
    constructor(sessionManager: ETPCore) {
        super();
        this.sessionManager = sessionManager;
    }
    role(): string { return ""; }
    start(){}
    stop(){}
    handleMessage(messageHeader, messageBody)
    {
        throw("Unsupported message {" + messageHeader.messageType +"}");
    }
    log(str){
        this.sessionManager.log(str);
    }
}

export class ETPCore extends events.EventEmitter {
    connection = null;
    name: string = "Energistics";
    schemas: any[];
    config:any = {};
    messageId: number = 0;
    schemaCache: etp.Util.SchemaCache = new etp.Util.SchemaCache();
    sessionId: string = NO_SESSION;
    binary: boolean = true;
    handlers: ProtocolHandler[]=[];
    store: IStore = null;
    stats = {
        blocks: 0,
        rows: 0,
        dataPoints: 0,
        bytesSent: 0,
        bytesReceived: 0,
        messagesSent: 0,
        messagesReceived: 0,
        connectDate: new Date(),
        firstDataBlock: null,
        lastDataBlock: null
    };

    constructor(configuration, store: IStore = null) {
        super();
        //this.handlers[0] = this;
        this.config = configuration;
    }

    // Send an acknowledgement of receipt of a message.
    acknowledge(messageId: number) {
        var header = this.createHeader(PROTOCOL.Core, MSG_CORE_ACKNOWLEDGE, messageId);
        var message = {};
        this.send(header, message);
    }

    // Close the current Session. Other party should respond by closing the socket.
    closeSession() {
        if (this.sessionId) {
            this.send(this.createHeader(PROTOCOL.Core, MSG_CORE_CLOSESESSION, 0), { sessionId: this.sessionId });
            this.sessionId = NO_SESSION;
        }
    }

    // Create a new header for a specific message kind and correlation id
    createHeader(protocol, messageType, correlationId) {
        return {  protocol: protocol, messageType: messageType, messageId: ++this.messageId, correlationId: correlationId };
    }

    // Are we in an active session.
    isInSession() {
        return this.connection != null && this.sessionId != NO_SESSION;
    }

    handleMessage(messageHeader, messageBody)
    {
    }

    log(message: string) {
        this.emit('log', message);
    }

    onReconnectSession(header, message) {
        console.log(JSON.stringify(message));
    }

    onCloseSession(header, message) {
        var i;
        for(i=0; i<this.handlers.length; i++) {
            if (this.handlers[i])
                this.handlers[i].stop();
        }
        this.log("Received CloseSession message for session {" + this.sessionId + "} Closing WebSocket.");
        this.sessionId=NO_SESSION;
        this.connection.close();
    }

    onAcknowledge (header, message, reader) {
    }

    registerHandler(id: number, handler: ProtocolHandler)
    {
        this.handlers[id]=handler;
        return handler;
    }

    send(header, message) {    }

    sendException(errorCode: number, errorMessage: string, correlationId: number=0) {
        var header = this.createHeader(PROTOCOL.Core, MSG_CORE_EXCEPTION, correlationId);
        var message = {errorCode: errorCode, errorMessage: errorMessage};
    }

}

export class ETPServer extends ETPCore {
    loggedMessageId: number = 0;
    start() {
        console.log("Starting ETPServer: ");
        this.sessionId = uuid.v4();
        if(this.config.traceMessages) {
            fs.mkdirSync(appRoot + '/trace/' + this.sessionId);
        }
        console.dir(this.config);
        console.log("Binary: ", this.binary);
        this.connection.on('message', this.onSocketMessage.bind(this));
        this.connection.on('close', this.onSocketClose.bind(this));
    }

    handleMessage(messageHeader, messageBody)
    {
        if(this.config.traceMessages) {
            var msgJson = JSON.stringify(["R", messageHeader, messageBody], null, 4);
            fs.writeFile(appRoot + "/trace/"  + this.sessionId + "/" + (++this.loggedMessageId) + ".json", msgJson);
        }
        if(messageHeader.protocol == PROTOCOL.Core) {
            switch(messageHeader.messageType) {
                case MSG_CORE_REQUESTSESSION:
                    this.onRequestSession(messageHeader, messageBody);
                    break;
                case MSG_CORE_CLOSESESSION:
                    this.onCloseSession(messageHeader, messageBody);
                    break;
                case MSG_CORE_EXCEPTION:
                    this.log("EXCEPTION: "+ messageBody.errorMessage);
                    break;
                default:
                    this.sendException(EINVALID_MESSAGE_TYPE, "Unsupported message {" + messageHeader.messageType + "}", messageHeader.messageId);
                    break;
            }
        }
        else {
            if(this.handlers[messageHeader.protocol])
                this.handlers[messageHeader.protocol].handleMessage(messageHeader, messageBody);
            else
                this.sendException(-1, "Unsupported protocol", messageHeader.messageId);
        }
    }

    onRequestSession(header, body) {
        var agreedProtocols=[];
        for(var i=0; i<body.requestedProtocols.length; i++) {
            var protocol = body.requestedProtocols[i];
            if (this.handlers[protocol.protocol] && this.handlers[protocol.protocol].role()==protocol.role) {
                agreedProtocols.push(protocol)
                //this.handlers[protocol.protocol].start();
            }
        }
        var msg = {
            applicationName: "ralf-server",
            sessionId: (this.sessionId),
            supportedProtocols: agreedProtocols
        };

        if (msg.supportedProtocols.length > 0) {
            this.log("Opening new session {" + msg.sessionId + "} with " + body.applicationName);
            this.send(this.createHeader(PROTOCOL.Core, MSG_CORE_OPENSESSION, header.messageId), msg);
        }
        else {
            var error = {errorCode:2, errorMessage:"No supported protocols."};
            this.send(this.createHeader(PROTOCOL.Core, MSG_CORE_EXCEPTION, header.messageId), error);
        }
        for(var i=0; i<agreedProtocols.length; i++) {
            var protocol = agreedProtocols[i];
            this.log("Using protocol: " + protocol.protocol + " in role of '" + protocol.role + "'");
            if (this.handlers[protocol.protocol].role()==protocol.role) {
                this.handlers[protocol.protocol].start();
            }
        }
    }

    onSocketClose(reasonCode, description) {
        this.log((new Date()) + ' Peer ' + this.connection.remoteAddress + ' disconnected.');
        this.log('--Reason Code [' + reasonCode + '] Description [' + description + ']');
        this.emit('disconnect');
    }

    onSocketMessage(message, flags) {
        this.stats.messagesReceived++;
        if (message.type === 'utf8' || (flags && (!flags.binary))) {
            this.binary = false;
            this.stats.bytesReceived += message.utf8Data.length;
            var parts = JSON.parse(message.utf8Data);
            this.handleMessage(parts[0], parts[1]);
        } else {
            this.stats.bytesReceived += message.binaryData.length;
            var reader = new avro.BinaryReader(this.schemaCache, new Uint8Array(message.binaryData)),
                header = reader.readDatum("Energistics.Datatypes.MessageHeader"),
                body = reader.readDatum(this.schemaCache.find(header.protocol, header.messageType));
            this.handleMessage(header, body);
        }
    }

    resume(){
    }

    send(header, message) {
        var dataToSend = null,
            encoder = null,
            schemaName = this.schemaCache.find(header.protocol, header.messageType).fullName;
        if(this.config.traceMessages) {
            var msgJson = JSON.stringify(["S", header, message], null, 4);
            fs.writeFile(appRoot + "/trace/"  + this.sessionId + "/" + (++this.loggedMessageId) + ".json", msgJson);
        }
        this.stats.messagesSent++;

        if (this.binary) {
            encoder = new avro.BinaryWriter(this.schemaCache);
            encoder.writeDatum("Energistics.Datatypes.MessageHeader", header);
            encoder.writeDatum(schemaName, message);
            dataToSend = encoder.getBuffer();

        } else {
            dataToSend = JSON.stringify([header, message]);
        }

        this.stats.bytesSent += dataToSend.length;
        this.connection.send(dataToSend, { binary: this.binary, mask: false });
    }
}

export class ETPClient extends ETPCore {
    host: string;

    constructor(config: any) {
        super(config);
    }

    connect(serverUrl, encoding) {
        if (serverUrl != null)
            this.host = serverUrl;
        encoding = encoding ? encoding : "binary";
        this.binary = (encoding === "binary");
        this.connection = new WebSocket(this.host + "?etp-encoding=" + encoding, "energistics-tp");
        if (this.binary)
            this.connection.binaryType = "arraybuffer";

        this.connection.onopen = (function (evt) {
            this.emit("connect", this.connection);
        }).bind(this);

        this.connection.onclose = this.onSocketClose.bind(this);

        this.connection.onmessage = this.onSocketMessage.bind(this);

        this.connection.onerror = (function (evt) {
            this.emit('log', 'Connection Error');
        }).bind(this);
    }

    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Core) {
            switch(messageHeader.messageType) {
                case MSG_CORE_OPENSESSION:
                    this.onOpenSession(messageHeader, messageBody);
                    break;
                case MSG_CORE_CLOSESESSION:
                    this.onCloseSession(messageHeader, messageBody);
                    break;
                case MSG_CORE_EXCEPTION:
                    this.log("EXCEPTION: "+ messageBody.errorMessage);
                    break;
                default:
                    throw("Unsupported message {" + messageHeader.messageType + "} in ETPClient")
            }
        }
        else {
            this.handlers[messageHeader.protocol].handleMessage(messageHeader, messageBody);
        }
    }

    onOpenSession(messageHeader, messageBody)
    {
        this.log("Opened Session {" + messageBody.sessionId + "} with " + this.host);
        this.sessionId = messageBody.sessionId;
        for (var i=0; i<messageBody.supportedProtocols.length; i++) {
            var prot = messageBody.supportedProtocols[i];
            this.log("Supported protocol: " + prot.protocol + " " +prot.role);
            //if( prot.initiation == this.handlers[prot.protocol].role() ) {
                this.handlers[prot.protocol].start();
            //}
        }
        this.emit('open');
    }

    onSocketClose(evt) {
        this.log((new Date()) + ' Peer ' + this.connection.url + ' disconnected.');
        this.log('--Reason Code [' + evt.code + '] Description [' + evt.reason + ']');
        this.emit('disconnect', this.connection);
    }

    onSocketMessage(msg) {
        var header = null;
        var message = null;
        this.stats.messagesReceived++;
        if (typeof (msg.data) == "object") {
            this.stats.bytesReceived += msg.data.byteLength;
            var reader = new avro.BinaryReader(this.schemaCache, new Uint8Array(msg.data));
            header = reader.readDatum("Energistics.Datatypes.MessageHeader");
            message = reader.readDatum(this.schemaCache.find(header.protocol, header.messageType));
        }
        else {
            this.stats.bytesReceived += msg.data.length;
            var data = JSON.parse(msg.data);
            header = data[0];
            message = data[1];
        }
        this.handleMessage(header, message);
    }

    requestSession(requestedProtocols, applicationName) {
        //this.capabilities = caps;
        var header = this.createHeader(PROTOCOL.Core, MSG_CORE_REQUESTSESSION, 0);
        var message = {
            requestedProtocols: requestedProtocols,
            applicationName: applicationName
        };
        this.send(header, message);
    }

    send(header, message) {
        this.stats.messagesSent++;
        var dataToSend = null, encoder;
        var schemaName = this.schemaCache.find(header.protocol, header.messageType).fullName;
        if (this.binary) {
            encoder = new avro.BinaryWriter(this.schemaCache);
            encoder.writeDatum("Energistics.Datatypes.MessageHeader", header);
            encoder.writeDatum(schemaName, message);
            dataToSend = encoder.getArrayBuffer();

        } else {
            dataToSend = JSON.stringify([header, message]);
        }

        this.stats.bytesSent += dataToSend.byteLength;
        this.connection.send(dataToSend);
    }
}

    /***
     * Implements the producer end of protocol 1
     */
export class Producer extends BaseProtocolHandler implements ProtocolHandler {
    dataSource:any;
    nextChannelId:number = 0;
    simpleProducer:boolean = false;

    role():string {
        return "producer";
    }

    constructor(sessionManager:ETPCore, dataSource: any) {
        super(sessionManager);
        this.dataSource = dataSource;
    }

    start() {
        this.dataSource.on('metadata', this.onMetadata.bind(this));
        this.dataSource.on('data', this.onData.bind(this));

        // No support for control messages, so just start streaming
        if (this.simpleProducer) {
            this.dataSource.describe(this.sessionManager.config.defaultSubscription, this.newChannelId, this.sessionManager.store);
            this.dataSource.simulateRealTime();
        }
    }

    stop() {
        if (this.dataSource)
            this.dataSource.stop();
        this.sessionManager = null;
    }

    handleMessage(messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case MSG_START:
                this.start();
                break;
            case MSG_CHANNEL_DESCRIBE :
                try {
                    this.log("Describe Request: " + messageBody.uris);
                    this.dataSource.describe(messageBody.uris, this.newChannelId.bind(this), this.sessionManager.store);
                }
                catch (exception) {
                    this.sessionManager.log(JSON.stringify(exception));
                    this.sessionManager.sendException(0, "Unable to describe URI " + messageBody.uris, messageHeader.messageId);
                }
                break;
            case MSG_CHANNEL_RANGE_REQUEST:
                break;
            case MSG_CHANNEL_START_STREAMING :
                this.dataSource.startStreaming(messageBody.channels);
                break;
            case MSG_CHANNEL_STOP_STREAMING :
                this.dataSource.stopStreaming(messageBody.channels);
                break;
            default:
                super.handleMessage(messageHeader, messageBody);
                break;
        }
    }

    /**
     * Handle channel data from the simulator, pass on to the consumer.
     * @param channelData
     */
    onData(channelData) {
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Data, MSG_CHANNEL_DATA, 0);
        this.sessionManager.send(header, channelData);
    }

    /** Handle new metadata coming from the simulator */
    onMetadata(channelMetadata) {
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Data, MSG_CHANNEL_METADATA, 0);
        this.sessionManager.send(header, channelMetadata);
    }

    /** Generate a unique channel ID for this session */
    newChannelId():number {
        return this.nextChannelId++;
    }
}

/**
 * Implements the consumer end of protocol 1
 */
export class Consumer extends BaseProtocolHandler {
    constructor(sessionManager:ETPCore) {
        super(sessionManager);
    }

    role() {
        return "consumer";
    }

    start() {
        super.start();
        this.sessionManager.log("Starting new Consumer.");
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Data, 0 /* Start is always 0 */, 0 /*No correlation ID */);
        var message = {role: "Consumer", maxMessageRate: 0, maxDataItems: 10};
        this.sessionManager.send(header, message);
    }

    describe(uris:string[]) {
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Data, MSG_CHANNEL_DESCRIBE, 0);
        this.sessionManager.send(header, {uris: uris});
    }

    handleMessage(messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case MSG_CHANNEL_METADATA:
                this.emit('metadata', messageBody);
                break;
            case MSG_CHANNEL_DATA:
                this.emit('data', messageBody);
                break;
            default:
                super.handleMessage(messageHeader, messageBody);
                break;
        }
    }

    startStreaming() {

    }
}
/**
 * Implements the producer end of protocol 2
 */
export class HistoryProducer extends Producer {
    constructor(sessionManager: ETPCore, dataSource: any) {
        super(sessionManager, dataSource);
    }
    role() { return "producer"; }

    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Channel_Tabular) {
            switch(messageHeader.messageType) {
                case MSG_HISTORICAL_REQUEST :
                    try {
                        this.dataSource.realTime = false;
                        this.dataSource.describe(messageBody.uri, this.newChannelId.bind(this), this.sessionManager.store);
                    }
                    catch(exception) {
                        this.log(JSON.stringify(exception));
                        this.sessionManager.sendException(0, "Unable to describe URI: " + messageBody.uri, messageHeader.messageId);
                    }
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
                    break;
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in HistoricalProducer");
        }
    }

    /**
     * Handle channel data from the simulator, pass on to the consumer.
     * @param channelData
     */
     onData(channelData) {
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Tabular, MSG_HISTORICAL_DATA, 0);
        this.sessionManager.send(header, channelData);
    }

    onMetadata(channelMetadata) {
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Tabular, MSG_HISTORICAL_METADATA, 0);
        this.sessionManager.send(header, channelMetadata);
    }
}

/**
 * Implements the consumer end of protocol 2
 */
export class HistoryConsumer extends BaseProtocolHandler {
    constructor(sessionManager: ETPCore) {
        super(sessionManager);
    }
    role() { return "consumer"; }
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Channel_Tabular) {
            switch(messageHeader.messageType) {
                case MSG_HISTORICAL_METADATA:
                    this.emit('metadata', messageBody);
                    break;
                case MSG_HISTORICAL_DATA:
                    this.emit('data', messageBody);
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in HistoricalConsumer")
        }
    }

    requestChannelData(subscription: string) {
        var header = this.sessionManager.createHeader(PROTOCOL.Channel_Tabular, MSG_HISTORICAL_REQUEST, 0);
        this.sessionManager.send(header, {uri: subscription, fromIndex: null, toIndex: null});
    }
}

/* Implements provider end of protocol 3 */
export class DiscoveryHandler extends BaseProtocolHandler {
    store: IStore = null;
    constructor(sessionManager: ETPCore, store: IStore) {
        super(sessionManager);
        this.store = store;
    }
    role(){ return "store"; }
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Discovery) {
            switch(messageHeader.messageType) {
                case MSG_DISCOVERY_GET :
                    this.log("Discovery: Get " + messageBody.uri);
                    if (this.store)
                        this.store.enum(messageBody.uri, resource_callback.bind({handler: this, header: messageHeader, body: messageBody}));
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in DiscoveryHandler")
        }
    }
}

function resource_callback(err, data) {
    if(err) {
        this.handler.log(err);
    }
    else if(data[0]) {
        // console.log(data);
        var resources;
        if (data[0].logCurveInfo) {
            resources = data[0].logCurveInfo.map(function(x, i, log){ return {name: x.mnemonic, uri: this._id + "/channel(" + x.mnemonic + ")"}}, data[0]);
        }
        else {
            resources = data;
        }
        var header = this.handler.sessionManager.createHeader(PROTOCOL.Discovery, MSG_DISCOVERY_RESOURCE, this.header.messageId);
        try {
            this.handler.sessionManager.send(header, {uri: this.body.uri, resources: resources});
        }
        catch (e) {
            this.handler.log(e);
        }
    }
}

/* Implements the consumer end of protocol 3 */
export class DiscoveryClient extends BaseProtocolHandler {
    constructor(sessionManager: ETPCore, store: IStore) {
        super(sessionManager);
    }
    start(){
    }
    stop(){}
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Discovery) {
            switch(messageHeader.messageType) {
                case MSG_DISCOVERY_RESOURCE :
                    this.emit("resource", messageBody);
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in DiscoveryHandler")
        }
    }

    get(uri: string) {
        var header = this.sessionManager.createHeader(PROTOCOL.Discovery, MSG_DISCOVERY_GET, 0);
        this.sessionManager.send(header, {uri: uri});
    }
}

function object_callback(err, data) {
    if(err) {
        this.handler.log(err);
    }
    else  {
        var components;
        var header = this.handler.sessionManager.createHeader(PROTOCOL.Object_Store, MSG_STORE_OBJECT, this.header.messageId);
        try {
            var dataBlock = [1,2,3,4];
            dataBlock[10000000]=1234;
            this.handler.sessionManager.send(header, { dataObjects: [{
                namespace: "wsmckenz",
                version:"0",
                objectType:'array',
                contentType:"binary",
                contentEncoding: "none",
                data: dataBlock}]});
        }
        catch (e) {
            this.handler.log(e);
        }
    }
}

/* Implements the server side of protocol 4 */
export class StoreHandler extends BaseProtocolHandler {
    store: IStore = null;
    constructor(sessionManager: ETPCore, store: IStore) {
        super(sessionManager);
        this.store = store;
    }
    role(){ return "store"; }
    start(){}
    stop(){}
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Object_Store) {
            switch(messageHeader.messageType) {
                case MSG_STORE_GET :
                    this.log("Get " + messageBody.uri + " from store.")
                    if (this.store)
                        this.store.get(messageBody.uri, object_callback.bind({handler: this, header: messageHeader, body: messageBody}));
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in StoreHandler")
        }
    }

}

/* Implements the consumer side of protocol 4 */
export class StoreClient extends BaseProtocolHandler {
    constructor(sessionManager: ETPCore, store: IStore) {
        super(sessionManager);
    }
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Object_Store) {
            switch(messageHeader.messageType) {
                case MSG_STORE_OBJECT:
                    this.emit("object", messageHeader, messageBody);
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in DiscoveryHandler")
        }
    }

    get(uri: string) {
        this.log("Getting " + uri + " from store");
        var header = this.sessionManager.createHeader(PROTOCOL.Object_Store, MSG_STORE_GET, 0);
        this.sessionManager.send(header, {uri: uri});
    }
}





