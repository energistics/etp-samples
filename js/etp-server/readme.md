#Node RaLF Server

This is an experimental implementation of a node.js server and html5 client for the Energistics Transfer protocol (ETP).
ETP is a proposed specification for streaming real-time data from oil field drilling and production facilities. It uses
websockets for transport and Apache Avro for serialization.

This implementation also uses mongodb for storage, although that is not part of the spec.

#Prerequisites
- Install Node from nodejs.org - v0.10 min required.
- Install Mongodb

#Installation
	c:\>mkdir ralfdemo
	c:\>cd ralfdemo

### To install from npm
	npm install node-ralf

### To install from source
	Clone the node folder from mercurial. Say, into c:\sf\rlfprototype-code-0\src\Node

	c:\ralfdemo>npm install d:\sf\rlfprototype-code-0\src\Node

#Running
	c:\ralfdemo>node bin/server
	Unzipped sample data

	simple-http-server Now Serving: ./ at http://localhost:8080/

	Wed May 08 2013 08:05:21 GMT-0500 (Central Daylight Time) RaLF Server is listening on port 8081

Now point your modern, html5-compliant browser at http://localhost:8080

#Options
The following can be passed as command line options when starting the server.

	Option | Default | Description
	:------ | ------- | --------
	--httpServer | true | Run the web server, set false if you just want the RaLF Websocket server                                     
	--httpPort=port | 8080 | Web Server Port
	--wsPort=port | 8081 | Websocket Port
	--schemas | lastest | Name of the RaLF schema file to use. Look in the schema folder for .avpr files, any can be used 
	--autoSubscribe | false| Start pushing data without a subscription
	--defaultSubscription | | Name of a Uri to use when auto-publishing, must correspond to a .json file in the data directory.
	--databaseConnectString | mongodb://localhost:27017/witsml | mongodb connection string
	--traceMessages | false | Creates a disk log of each message sent and received by the server.
	--traceDirectory | trace | Name of the folder to hold the trace files.
	--help | | print this information

#Known Issues
	- Does not currently support 5 minute ring buffer.
	- Only supports subscription on log objects 
