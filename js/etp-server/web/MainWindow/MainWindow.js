(function ($) { // Hide scope, no $ conflict

    var Energistics = require("../../lib/Energistics"),
	Avro = Energistics.Avro;



    $.widget("ui.MainWindow", {

        _rlf: null,
        _channels: [],
        _tree: null,

        options: {
            navigator: true,
            templatePath: "web/MainWindow/template.html",
            toolbar: true
        },

        connect: function (event) {
            var self = event.data.self;
            var rlf = self._rlf;
            if (rlf.isInSession()) {
                self.log("Sending CloseSession for {" + rlf.sessionId + "}");
                rlf.closeSession();
                $("#buttonConnect").html("Connect");
            }
            else {
                var encoding = $("#etp-encoding")[0].value;
                rlf.connect($("#socket-server")[0].value, encoding);
                $("#buttonConnect").html("Disconnect");
            }
        },

        createTree: function(Y) {
            this._tree = new Y.TreeView({ container: '#wellTree' });

            // Fetch children when node opened
            this._tree.after('open', this.get_children.bind(this));

            // Render the treeview inside the #treeview element.
            this._tree.render();

        },

        createMenu: function(Y) {
            var cmenu = new Y.ContextMenuView({
                menuItems: [
                    { label: "Describe", value:"d" },
                    { label: "History", value:"h" },
                    { label: "Info", value:"i" }
                ],
                trigger: {
                    node: Y.one("#wellTree"),
                    target:  '.yui3-treeview-row'
                }
            });

            cmenu.handler = this;

            cmenu.after('selectedMenuChange',  function(e) {
                var uri = this.get('contextTarget')._node.attributes['data-node-id'].value;
                Y.log(uri);
                switch(e.newVal.menuItem.value) {
                    case 'd':
                        this.handler._rlf.handlers[1].describe([uri]);
                        break;
                    case 'h':
                        // Get channel as history
                        this.handler._rlf.handlers[3].requestChannelData(uri);
                        $("#DemoTabs").tabs('select', 1);
                        break;
                    case 'i':
                        // Get object from store
                        this.handler._rlf.handlers[5].requestChannelData(uri);
                        $("#DemoTabs").tabs('select', 2);
                        break;
                }
            });
        },

        createChannelTable: function(Y) {

            // A table from data with keys that work fine as column names
            this.activeChannelsView = new Y.DataTable({
                columns: ["mnemonic", "uom", "value"],
                data   : []
            }).render("#channelTree");

        },

        createStatisticsTable: function(Y) {

            // A table from data with keys that work fine as column names
            this.statisticsView = new Y.DataTable({
                columns: [
                    {key: "id", width: '100px'},
                    {key: "uom", width: '50px'},
                    {key: "value", width: '200px'}
                ],
                data   : [

                ]
            }).render("#sessionStatistics");

        },

        getArray: function(evt) {
            evt.data.self._rlf.handlers[5].get('surface');
        },

        init: function () {
			YUI({gallery: 'gallery-2013.06.20-02-07'}).use('gallery-sm-treeview', this.createTree.bind(this));
            YUI({gallery: 'gallery-2013.01.23-21-59'}).use('node', 'cssfonts', 'gallery-contextmenu-view', this.createMenu.bind(this));
            // datatable-base includes only basic table rendering, but in this case,
            // that's enough.
            YUI().use("datatable-base", this.createChannelTable.bind(this));
            YUI().use("datatable-base", 'datatable-column-widths', this.createStatisticsTable.bind(this));
            $("#DemoTabs").tabs();
            $('#LogViewer1').logviewer({  anchorToWindow: false, topDepth: 2000, bottomDepth: 5000 });
            $('#logTransfer').logtransfer({  });

            // Point the websocket client to our source domain.
            this._rlf = new Energistics.ETPClient(this);
            this._rlf.on('log', this.log.bind(this));
            this._rlf.on('connect', this.on_ws_connect.bind(this));
            this._rlf.on('disconnect', this.on_ws_disconnect.bind(this));
            this._rlf.on('close', this.on_close.bind(this));
            this._rlf.on('open', this.on_open.bind(this));

            this._rlf.registerHandler(1, new Energistics.Consumer(this._rlf));
            this._rlf.handlers[1].on('metadata', this.on_channel_metadata.bind(this));
            this._rlf.handlers[1].on('data', this.on_channel_data_block.bind(this));

            //this._rlf.registerHandler(2, new Energistics.Subscriber(this._rlf));

            this._rlf.registerHandler(2, new Energistics.HistoryConsumer(this._rlf));
            $("#logTransfer").data().logtransfer.setProtocolHandler(this._rlf.handlers[2]);

            this._rlf.registerHandler(3, new Energistics.DiscoveryClient(this._rlf));
            this._rlf.handlers[3].on("resource", this.on_discover_resource.bind(this));

            this._rlf.registerHandler(4, new Energistics.StoreClient(this._rlf));
            this._rlf.handlers[4].on("object", this.on_data_object.bind(this));

            // Point the websocket client to our source domain.
            $('#socket-server')[0].value = 'ws://' + document.domain + ":8081";
            $("#buttonSubscribe").bind("click", { self: this }, this.subscribe);
            $("#buttonConnect").bind("click", { self: this }, this.connect);
            $("#testArray").bind("click", { self: this }, this.getArray);

            setInterval(this._updateUi.bind(this), 1000);

            var self = this, options = self.options;
            self.logViewerFrame = $(".logviewer-frame", self.element);
            self.logViewerNavigator = $(".logviewer-navigator", this.element);
            self.toolbar = $(".logviewer-toolbar", this.element);
            self.logViewerNavigator.accordion({ fillSpace: true });
            self.scaleBar = $(".logviewer-scale", this.element)
				.slider({
				    width: "400px", min: 0, max: 10, increment: 1,
				    slide: function (event, ui) {
				        $(this).parents(".logviewer").logviewer("setScale", ui.value);
				    }
				});

            $(".logviewer-right", this.element).css({ width: this.element.width() - 300 });
            self.logViewerContent = $(".logviewer-content", this.element);
            self.logGaugeContent = $(".logviewer-gauges", this.element);

            $(".slider-range", this.element).slider({
                orientation: "vertical",
                range: false,
                max: -options.topDepth,
                min: -options.bottomDepth + this.element.innerHeight() - 200,
                values: [-options.currentDepth],
                slide: function (event, ui) {
                    $(this).parents(".logviewer").logviewer("option", "currentDepth", -ui.values[0]);
                }
            });

            $(window).resize(function () {
                $(document).find(".logviewer").each(function () {
                    total = $(this).width();
                    nav = $(this).find(".logviewer-navigator").width();
                    $(document).find(".logviewer-right").css('width', (total - nav - 3) + "px");
                });
            });

            total = $(this.element).width();
            nav = $(".logviewer-navigator").width();
            $(".logviewer-right").css('width', (total - nav -1) + "px");

        },

        on_ws_connect: function (socket) {
            $("#buttonConnect").html("Disconnect");
            this.dataRows = this.dataPoints = this.dataBytes = this.startStreaming = this.endStreaming = 0;
            this.log('Socket Status: ' + socket.readyState + ' (open)');
            this.log("Requesting Consumer session with {" + $('#socket-server')[0].value + "}");
			var thisVersion = {major: 0, minor: 0, revision: 1, patch: 42};			
            this._rlf.requestSession([
			{protocol: 1, protocolVersion: thisVersion, role: "producer"}, 
			{protocol: 2, protocolVersion: thisVersion, role: "producer"}, 
			{protocol: 3, protocolVersion: thisVersion, role: "store"}, 
			{protocol: 4, protocolVersion: thisVersion, role: "store"}], 
			"ralf-html5-logviewer");
        },

        on_ws_disconnect: function (socket) {
            $("#buttonConnect").html("Connect");
            this.activeChannelsView.data.reset();
            $("#LogViewer1").data().logviewer.clear();
            this._tree.rootNode.children.map(function(node){node.remove();});
            this.log('Socket Status: ' + socket.readyState + ' (Closed)');
        },

        subscribe: function (evt) {
            evt.data.self._rlf.handlers[2].subscribe($(".ltSubscription")[0].value);
        },

        log: function (msg) {
            $('#logViewerLog').append('<p class="event">' + msg + '</p>');
            //$("#logViewerLog").scrollTop($("#ltLogWindow")[0].scrollHeight);
        },

        on_discover_resource: function(message) {
            this.dataPoints += message.resources.length;
            var folder = this._tree.getNodeById(message.uri);
            if ( !folder )
                folder = this._tree.rootNode;

            var items=message.resources;

            items = items.map(function(x){ return {label: x.name, id: x.uri, canHaveChildren: true, state: {open: false} }});

            if(items && items.length > 0) {
                var newone = folder.append(items);
            }

            //if($('#autoLoadTree').val() == "on") {
            //    message.components.map(this.auto_get_children.bind(this));
            //}
        },

        auto_get_children: function(component) {
            if(this._rlf && this._rlf.handlers[3]) {
                this._rlf.handlers[3].get(component.uri);
            }
        },

        get_children: function(evt) {
            if(this._rlf && this._rlf.handlers[4] && evt.node && !evt.node.gotChildren) {
                evt.node.gotChildren = true;
                this._rlf.handlers[3].get(evt.node.id);
            }
        },

        on_channel_data_block: function (message) {
            this.dataPoints += message.data.length;
            if((new Date().valueOf()-this.endStreaming) > 500) {
                this.endStreaming = new Date().valueOf();
                for (var i = 0; i < message.data.length; i++) {
                    var thisChannel = this._channels[message.data[i].channelId];
                    if (thisChannel !== undefined) {
                        var itemName = Object.getOwnPropertyNames(message.data[i].value.item)[0];
                        var value = message.data[i].value.item[itemName];
                        var numericValue = new Number(value).toFixed(2);
                        try {
                            this.activeChannelsView.data.item(i).set("value", value);
                        }
                        catch(e){}
                        if(thisChannel.gauge !== undefined) {
                            thisChannel.gauge.refresh(numericValue);
                        }
                    }
                }
            }
            else {

            }
        },

        on_channel_metadata: function (message) {
            this.startStreaming = this.endStreaming = new Date().valueOf();

            for (var i = 0; i < message.channels.length; i++) {
                var thisChannel = message.channels[i];
                this._channels[thisChannel.channelID] = thisChannel;
                if (!thisChannel.mnemonic)
                    thisChannel.mnemonic = thisChannel.channelUri.split(/[\/()]+/).reverse()[1];
                this.log(thisChannel.mnemonic);
                this.activeChannelsView.data.add(thisChannel);
                var logviewer = $('#LogViewer1').data('logviewer');
                switch(thisChannel.mnemonic)
                {
                    case "HKLD_MAX":
                        logviewer.addGauge(thisChannel, logviewer.gaugeStyle1);
                        break;
                    case "WOB_MAX":
                        logviewer.addGauge(thisChannel, logviewer.gaugeStyle2);
                        break;
                    case "TORQUE_AVG":
                        logviewer.addGauge(thisChannel, logviewer.gaugeStyle3);
                        break;
                    case "Temperature":
                    case "Pressure":
                    case "Altitude":
                    case "Real Altitude":
                        logviewer.addGauge(thisChannel, logviewer.gaugeStyle1);
                        break;
                }
            }
        },

        on_close: function (message) {
        },

        on_data_object: function(header, message) {
            this.log(message.dataObjects[0].objectType);
        },

        on_open: function () {
            this._rlf.handlers[1].start();
            //if (this._rlf.handlers[4].supported)
                this._rlf.handlers[3].get("/");
            //else
            //    this._tree.rootNode.append({label: "Discovery not supported", canHaveChildren: false});
        },

        _create: function () {
            this.element.addClass("MainWindow");
            this.element.load(this.options.templatePath, function () {
                $(this).MainWindow("init");
            });
        },

        _setOption: function (key, value) {
            var self = this,
                options = self.options;

            switch (key) {
            }

            $.Widget.prototype._setOption.apply(self, arguments);

            if (resize) {
                self._size();
            }
        },

        _setStatistic: function(name, value) {
            var model = this.statisticsView.data.getById(name);
            if(!model) {
                model = this.statisticsView.data.add({id: name, value: value});
            }
            else {
                model.set("value", value);
            }
        },

        _updateUi: function () {
            this._setStatistic("Time", (this.endStreaming - this.startStreaming) / 1000.0);
            this._setStatistic("Depth", this.depth);
            this._setStatistic("dataRows", this.dataRows);
            this._setStatistic("Data Points", this.dataPoints);
            this._setStatistic("Bytes Sent", this._rlf.stats.bytesSent);
            this._setStatistic("Bytes Received", this._rlf.stats.bytesReceived);
            this._setStatistic("Messages Sent", this._rlf.stats.messagesSent);
            this._setStatistic("Messages Received", this._rlf.stats.messagesReceived);
        }
    });

})(jQuery);
