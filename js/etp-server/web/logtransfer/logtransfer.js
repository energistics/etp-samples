(function ($) { // Hide scope, no $ conflict

    Energistics = require("../../lib/Energistics");

    $.widget("ui.logtransfer", {

        handler: null,
        dataBlocks: 0,
        dataRows: 0,
        dataPoints: 0,
        dataBytes: 0,

        // Rough timing mechanism.
        startStreaming: 0,
        endStreaming: 0,
        transferTime: 0,

        options: {
            templatePath: "web/logtransfer/template.html",
            // milliseconds between refreshes of the data table.
            updateInterval: 500
        },

        _create: function () {
            this.element.load(this.options.templatePath, function () {
                $(this).logtransfer("init");
            });
        },

        createTransferInfo: function(Y) {
            // A table from data with keys that work fine as column names
            this.transferInfo = new Y.DataTable({
                columns: [
                    {key: "id", width: '100px'},
                    {key: "value", width: '400px'}
                ],
                data   : []
            }).render("#transferInfo");
        },

        init: function () {
            // TODO - don't know the size of the log, so cannot make a progress bar.
            $("#progressBar").progressbar({value: 0});
            setInterval(this._updateUi.bind(this), this.options.updateInterval);
            YUI().use("datatable-base", 'datatable-column-widths', this.createTransferInfo.bind(this));
        },

        _setStatistic: function(name, value) {
            if(this.transferInfo && this.transferInfo.data) {
                // model here means the YIU concept of one row of data in the table.
                var model = this.transferInfo.data.getById(name);
                if(!model) {
                    model = this.transferInfo.data.add({id: name, value: value});
                }
                else {
                    model.set("value", value);
                }
            }
        },

        on_channel_data_block: function (message, header) {
            this.dataBlocks++;
            this.dataRows += message.data.length;
            this.dataPoints += (message.channels.length * message.data.length);
            this.endStreaming = new Date();
            this.transferTime = (this.endStreaming.valueOf()-this.startStreaming.valueOf()) / 1000.0;
        },

        /* TODO Assumption is that only one transfer is going on at a time, when we get new metadata we
           TODO re-initialize the UI. In fact, multiple transfers can be going on at once.
         */
        on_channel_metadata: function (message) {
            $(".dataChannels").val(message.channels.length);
            this.dataBlocks = this.dataRows = this.dataPoints = 0;
            this.startStreaming = this.endStreaming = new Date();
        },

        setProtocolHandler: function(handler) {
            // Should only happen once.
            if ( handler  && this.handler === null) {
                this.handler = handler;
                handler.on('metadata', this.on_channel_metadata.bind(this));
                handler.on('data', this.on_channel_data_block.bind(this));
            }
        },

        _updateUi: function () {
            this._setStatistic("Data Blocks", this.dataBlocks);
            this._setStatistic("Data Rows", this.dataRows);
            this._setStatistic("Data Points", this.dataPoints);
            this._setStatistic("Start Time", this.startStreaming);
            this._setStatistic("End Time", this.endStreaming);
            this._setStatistic("Transfer Time", this.transferTime.toFixed(1));
            this._setStatistic("Points / Sec", (this.dataPoints / this.transferTime).toFixed(0));
        }

    });

})(jQuery);
