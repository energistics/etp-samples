(function ($) { // Hide scope, no $ conflict

    $.widget("ui.logviewer", {

        _i: 0,
        _ALL: "100%",
        _HALF: "50%",
        _gridMajor: "grid-major",
        _tracks: {},

        options: {
            anchorToWindow: true,
            bottomDepth: 4000,
            currentDepth: 2000,
            navigator: true,
            scale: 1200,
            selectedTrack: this._i,
            templatePath: "web/logviewer/template.html",
            topDepth: 100,
            units: "feet"
        },

        _addCurve: function (track, svg, path) {
            var g = track.find(".log-group");
            if (g.length == 0) {
                var parent = track.find(".scale-group");
                g = svg.group(parent, { class_: ".log-group" });
            }
            return svg.path(g, path, { class_: "log-trace" });
        },

        addCurve: function () {
            var track = $(".log-track-selected", this.element);
            var svg = track.children(".log-track-detail").svg("get");
            var path = svg.createPath();
            path.move(Math.random() * 100.0, this.options.topDepth);
            for (i = this.options.topDepth; i < this.options.bottomDepth; i += 10) {
                path.line(5 + (Math.random() * 180.0), i);
            }
            return this._addCurve(track, svg, path);
        },

        addCurveFromWitsmlLog: function (track, log, depthIndex, traceIndex) {
            var svg = track.children(".log-track-detail").svg("get");
            var path = svg.createPath();
            var nodes = log.logs.log.logData.data.map(function (str) { return str.split(","); });
            var nodeCount = nodes.length;
            path.move(nodes[0][traceIndex], nodes[0][depthIndex]);
            for (i = 0; i < nodeCount; i++) {
                path.line(nodes[i][traceIndex], nodes[i][depthIndex]);
            }
            return this._addCurve(track, svg, path);
        },

        addLogTrack: function (className) {
            track = this._addTrack("log-track");
            track.header.append(track.attr("id"));
            track.click(function () {
                $(this).siblings(".log-track").removeClass("log-track-selected");
                $(this).addClass("log-track-selected");
            });
            track.click();

            return track;
        },

        addGauge: function (channelMetadata, factory) {
            this._i++;
            var newId = "gauge" + this._i;
            var gauges= this.logGaugeContent;
            var canvas = $('<canvas id="' + newId + '" class="horizontal" width="150" height="150"></canvas>').appendTo(gauges);
            var gauge = factory(newId);
            gauge.props.dialTitle = channelMetadata.mnemonic;
            gauge.props.dialSubTitle = channelMetadata.uom;
            channelMetadata.gauge=gauge;
            gauge.refresh((gauge.props.maxValue+gauge.props.minValue) / 2);
            return gauge;
        },

        _addTrack: function (className) {
            this._i++;
            newId = "Track" + this._i;
            this.selectedTrack = newId;
            var content = this.logViewerContent;
            //content.width(content.width() + 200);
            var track = this._new(className ? className : "log-track", content).attr("id", newId);
            track.height("780px");
            track.header = this._new("log-track-header", track);
            track.detail = this._new("log-track-detail", track);
            track.detail.height(track.height() - track.header.height());

            track.detail.svg({ loadURL: "web/logviewer/track.xml", onLoad: function (svg) {
                    $(this).parents(".logviewer").logviewer("configureTrack", svg, $(this).parent());
                }
            });

            return track;
        },

        addDepthTrack: function (index) {
            track = this._addTrack("depth-track");
            track.header.append("Depth<br><br>(" + this.options.units + ")");
            return track;
        },

        // Clear all content when we disconnect from a data source
        clear: function() {
            this.logGaugeContent.empty();
            $(".log-trace", this).empty();
            return this;
        },

        configureTrack: function (svg, thisTrack) {
            //
            var retVal = svg.configure({
                //class: "track-svg",
                width: "100%",
                height: this.options.bottomDepth + 100,
                transform: "scale(1,1)"
            },
				true);
            var g = svg.group({ class_: "scale-group", transform: "scale(1,1)" });
            var grd = svg.group(g, { class_: "grid-group" });

            var logViewer = $(this).parents(".logviewer");

            // some of this is not supported by the svg plugin, so
            // we need to get access to the actual svg element.
            var svgElement = svg.root();
            svgElement.style.position = "absolute";
            svgElement.style.top = -this.options.currentDepth + "px";
            var actualWidth = svgElement.scrollWidth;

            if (thisTrack.hasClass("log-track")) {
                var xMinorDivs = 10,
					xMajorDiv = 2,
					xMinorWidth = actualWidth / xMinorDivs,
					yMinorWidth = this.options.scale / 60,
					yMajorWidth = this.options.scale / 12;

                for (i = 1; i < xMinorDivs; ++i) {
                    svg.line(grd, i * xMinorWidth, 0, i * xMinorWidth, "100%", { class_: "grid-minor" });
                }
                svg.line(grd, "50%", 0, "50%", "100%", { class_: this._gridMajor });

                for (i = yMinorWidth; i < this.options.bottomDepth; i += yMinorWidth) {
                    svg.line(grd, 0, i, "100%", i, { class_: "grid-minor" });
                }
                for (i = yMajorWidth; i <= this.options.bottomDepth; i += yMajorWidth) {
                    svg.line(grd, 0, i, "100%", i, { class_: this._gridMajor });
                }
            }
            else {
                for (var i = 100; i <= this.options.bottomDepth; i += 100) {
                    svg.text(grd, "50%", i, i + "ft", { class_: this._gridMajor });
                }
            }
            return retVal;
        },

        _createButton: function (viewer, selector, command, option) {
            $(selector).bind("click", { myViewer: viewer }, function (event) {
                $(event.data.myViewer).logviewer(command, option);
            });
        },

        _create: function () {
            this.element.addClass("logviewer");
            this.element.load(this.options.templatePath, this.init.bind(this));
        },

        get: function () {
            return $(this);
        },

        _getCurrentTransform: function (scale) {
            var top = -$("#top").val();
            retVal = "scale(1," + scale + ")" + "translate(0," + 0 + ")";
            return retVal;
        },

        init: function () {

            var self = this, options = self.options;
            self.logViewerContent = $(".logviewer-content", this.element);
            self.logGaugeContent = $(".logviewer-gauges", this.element);

            $(".slider-range", this.element).slider({
                orientation: "vertical",
                range: false,
                max: -options.topDepth,
                min: -options.bottomDepth + this.element.innerHeight() - 200,
                values: [-options.currentDepth],
                slide: function (event, ui) {
                    $(this).parents(".logviewer").logviewer("option", "currentDepth", -ui.values[0]);
                }
            });

            $(window).resize(function () {
                $(document).find(".logviewer").each(function () {
                    total = $(this).width();
                    nav = $(this).find(".logviewer-navigator").width();
                    $(document).find(".logviewer-right").css('width', (total - nav - 3) + "px");
                });
            });

            total = $(this.element).width();
            nav = $(".logviewer-navigator").width();
            $(".logviewer-right").css('width', (total - nav -1) + "px");

            this.addLogTrack();
            this.addLogTrack();
            this.addDepthTrack();
            this.addLogTrack();
            this.addLogTrack();

        },

        gaugeStyle1: function(id) {
            var gauge = new AquaGauge(id);
            setupSmallGauge(gauge);
            gauge.props.dialColor = "rgba(120, 180, 110, 0.8)";
            gauge.props.dialGradientColor = "#fff";
            gauge.props.dialScaleFont = "bold 10px Trebuchet MS";
            gauge.props.dialTitleTextFont = "bold 12px Calibri";
            gauge.props.dialValueTextFont = "bold 14px Arial Black";
            gauge.props.rimWidth = "3";
            gauge.props.dialSubTitleTextFont = "10px Tahoma";
            gauge.props.pointerColor = "red";
            gauge.props.pointerGradientColor = "maroon";
            gauge.props.maxValue = 100;
            gauge.props.noOfDivisions = 10;
            gauge.refresh(45);
            return gauge;
        },

        gaugeStyle2: function(id) {
            var gauge = new AquaGauge(id);
            gauge.props.minValue = -50;
            gauge.props.maxValue = 150;
            gauge.props.noOfDivisions = 4;
            gauge.props.noOfSubDivisions = 5;
            gauge.props.showMinorScaleValue = true;
            gauge.refresh(0);
            return gauge;
        },

        gaugeStyle3: function(id) {
            var gauge = new AquaGauge(id);
            gauge.props.maxValue = 5.0;
            gauge.props.rangeSegments = [];
            gauge.props.dialColor = "#fff";
            gauge.props.dialGradientColor = "#fff";
            gauge.props.noOfDivisions = 4;
            gauge.props.noOfSubDivisions = 8;
            gauge.props.majorDivisionColor = "black";
            gauge.props.minorDivisionColor = "red";
            gauge.props.dialTitleTextFont = "bold 15px Calibri";
            gauge.props.dialTitleTextColor = "maroon";
            gauge.props.dialValueTextFont = "bold 14px Arial Black";
            gauge.props.dialSubTitleTextFont = "bold 13px Calibri";
            gauge.props.showGlossiness = false;
            gauge.refresh(0);
            return gauge;
        },

        _new: function (classes, parent) {
            return $("<div></div>").addClass(classes).appendTo(parent)
        },

        _scrollTo: function (newDepth) {
            newDepth = Math.max(newDepth, this.options.topDepth);
            newDepth = Math.min(newDepth, this.options.bottomDepth);
            this.options.currentDepth = newDepth;
            $("svg", this.element).each(function () {
                $(this).css('top', -newDepth);
            })
        },

        _setOption: function (key, value) {
            switch (key) {
                case "currentDepth":
                    return this._scrollTo(value);
            }

            $.Widget.prototype._setOption.apply(self, arguments);
        },

        setScale: function (relative) {
            _transform = this._getCurrentTransform(relative);
            $(".scale-group").each(function (index, el) {
                el.setAttribute("transform", _transform);
            });
        }
    });
})(jQuery);
