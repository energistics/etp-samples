/**
 *
 * @type {*}
 */
var path = require('path');
var fs = require('fs');
argv = require('optimist')
    .default(
    {
        schemas: '../Schemas',
        outputProtocolFile: "schemas/ralf.avpr",
        outputJavaScriptFile: "lib/ralf-schemas.js",
        verbose: false
    }
    ).argv;

/**
 * @function isComplex
 * @param name
 * @returns {boolean}
 */
function isComplex(name) {
    return ['boolean', 'int', 'long', 'double', 'float', 'string', 'fixed', 'bytes', 'null', 'enum'].indexOf(name) < 0;
}

function typeList(schema) {
    switch (typeof schema) {
        case "string":
            if (isComplex(schema))
                this.depends.push(schema);
            break;
        case "object":
            if (schema.fields)
                schema.fields.map(typeList, this);
            else if (schema instanceof Array)
                schema.map(typeList, this);
            else if (schema.type == 'array')
                typeList.bind(this)(schema.items);
            else if (schema.type == 'map')
                typeList.bind(this)(schema.values);
            else if (schema.type)
                typeList.bind(this)(schema.type);
            else
                throw ("Unknown schema type in typeList()");
            break;
    }

    return this;
}


var walk = function (dir, done) {
    var results = [];
    fs.readdir(dir, function (err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function (file) {
            file = dir + '/' + file;
            fs.stat(file, function (err, stat) {
                if (stat && stat.isDirectory()) {
                    walk(file, function (err, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    results.push(file);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};

var schemas = [];
var schemaFileNames = []; //fs.readdirSync(argv.schemas);

var protocol = {
    namespace: "energistics",
    protocol: "exp",
    version: "",
    types: []
};

walk(argv.schemas, function (err, results) {
    if (err) throw err;
    if (argv.verbose)
        console.log(results);
    schemaFileNames = results;

    for (var i = 0; i < schemaFileNames.length; ++i)
        if (schemaFileNames[i].match(/.*\.avsc/)) {
            if(argv.verbose)
                console.log(schemaFileNames[i]);
            var schema = JSON.parse(fs.readFileSync(schemaFileNames[i], 'utf8'));
            schema.fullName = schema.namespace + "." + schema.name;
            schemas[schema.fullName] = schema;
            schemas.push(schema);
        }

    for (var i = 0; i < schemas.length; i++) {
        if(argv.verbose)
            console.log(schemas[i].fullName);
        schemas[i].depends = [];
        typeList.bind(schemas[i])(schemas[i]);
        if(argv.verbose)
            console.log(schemas[i].depends);
    }

    schemas.map(function (schema) {
        if(argv.verbose)
            console.log(schema.name);
        protocol.types.push(schema);
        var parts=[];
        if(schema.namespace) {
            parts=schema.namespace.split(".");
        }
        //parts.push(schema.name);
        var currentObject = protocol;
        while (parts.length > 0)
        {
            if(!currentObject[parts[0]])
            currentObject[parts[0]]={};
            currentObject = currentObject[parts.shift()];
        }
        currentObject[schema.name]=schema;
        console.dir(protocol);
    });

    // Create as .avpr
    fdw = fs.openSync(argv.outputProtocolFile, 'w');
    fs.writeSync(fdw, JSON.stringify(protocol));
    fs.close(fdw);

    // Create as javascript object for Node
    fdw = fs.openSync(argv.outputJavaScriptFile, 'w');
    fs.writeSync(fdw, "var RalfSchemas = JSON.parse('"+ JSON.stringify(protocol) + "');\n\nmodule.exports=RalfSchemas;\n");
    fs.close(fdw);
});
