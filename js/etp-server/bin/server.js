///<reference path='../lib/reference/node.d.ts'/>
///<reference path='../lib/Energistics.ts'/>
var path = require('path'), http = require('http'), WebSocketServer = require('websocket').server, Energistics = require('../lib/Energistics'), argv = require('optimist').usage("\nETP Server for Node.js").default({
    httpServer: true,
    httpPort: 8080,
    wsPort: 8081,
    protocol: 'energistics-tp',
    schemas: 'ralf',
    simpleProducer: false,
    traceMessages: false,
    traceDirectory: 'trace',
    storeProvider: '../lib/MongoStore',
    databaseConnectString: 'mongodb://localhost:27017/witsml',
    simulator: '../lib/StoreSimulator',
    defaultUri: "log(1420MEOM59)"
}).argv;

if (argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

var StoreProvider = require(argv.storeProvider);
var store = new StoreProvider(argv.databaseConnectString);
var simulator = require(argv.simulator);
var realTimeDataSource = new simulator();

// Run the web server
if (argv.httpServer) {
    var webserver = require('simple-http-server');
    webserver.run({ port: argv.httpPort, directory: path.dirname(path.dirname(module.filename)) });
}

// Create another server for the websocket traffic.
var server = http.createServer(function (request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(argv.wsPort, function () {
    console.log((new Date()) + ' Server is listening on port ' + argv.wsPort);
});

var wsServer = new WebSocketServer({
    httpServer: server
});

var g_Connections = [];

wsServer.on('request', function (request) {
    for (var queryParameter in request.resourceURL.query) {
        request.httpRequest.headers[queryParameter.toLowerCase()] = request.resourceURL.query[queryParameter];
    }

    if (request.httpRequest.headers['etp-sessionid'] != null) {
        if (ralf = g_Connections[request.httpRequest.headers['etp-sessionid']] != null) {
            ralf.connection = request.accept(argv.protocol, request.origin);
            ralf.resume();
        } else {
            request.reject(410);
        }
    } else {
        var ralf = new Energistics.ETPServer(argv, store);
        var producer = ralf.registerHandler(1, new Energistics.Producer(ralf, realTimeDataSource));
        producer.simpleProducer = argv.simpleProducer;

        // simpleProducer causes us to behave like a dumb server, supporting only
        // protocols 0 and part of 1
        if (!argv.simpleProducer) {
            ralf.registerHandler(2, new Energistics.HistoryProducer(ralf, realTimeDataSource));
            ralf.registerHandler(3, new Energistics.DiscoveryHandler(ralf, store));
            ralf.registerHandler(4, new Energistics.StoreHandler(ralf, store));
        }

        // Server-side, all messages go to console.
        ralf.on('log', function (msg) {
            console.log(msg);
        });

        ralf.binary = request.httpRequest.headers['etp-encoding'] === 'binary';
        console.log((new Date()) + ': Connection accepted.');
        ralf.connection = request.accept(argv.protocol, request.origin);
        ralf.start();
        g_Connections[ralf.sessionId] = ralf;
    }
});
