///<reference path='../lib/reference/node.d.ts'/>
///<reference path='../lib/Energistics.ts'/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var path = require('path'), http = require('http'), perfmon = require('perfmon'), WebSocketServer = require('websocket').server, argv = require('optimist').usage("\nPerfMon Producer for Node.js").default({
    httpServer: true,
    httpPort: 8080,
    wsPort: 8081,
    protocol: 'energistics-tp',
    schemas: 'ralf',
    simpleProducer: false,
    traceMessages: false,
    traceDirectory: 'trace',
    defaultUri: "\\processor(_total)\\% processor time"
}).argv;

var Energistics = require('../lib/Energistics');

if (argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

// Create another server for the websocket traffic.
var server = http.createServer(function (request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(argv.wsPort, function () {
    console.log((new Date()) + ' Server is listening on port ' + argv.wsPort);
});

var wsServer = new WebSocketServer({ httpServer: server });

var g_Connections = [];

wsServer.on('request', function (request) {
    for (var queryParameter in request.resourceURL.query) {
        request.httpRequest.headers[queryParameter.toLowerCase()] = request.resourceURL.query[queryParameter];
    }

    if (request.httpRequest.headers['etp-sessionid'] != null) {
        request.reject(410);
    } else {
        var ralf = new Energistics.ETPServer(argv, null);
        var producer = new PerfMonProducer(ralf);
        ralf.registerHandler(1, producer);
        producer.simpleProducer = argv.simpleProducer;

        // Server-side, all messages go to console.
        ralf.on('log', function (msg) {
            console.log(msg);
        });
        ralf.binary = request.httpRequest.headers['etp-encoding'] === 'binary';
        console.log((new Date()) + ': Connection accepted.');
        ralf.connection = request.accept(argv.protocol, request.origin);
        ralf.start();
        g_Connections[ralf.sessionId] = ralf;
    }
});

var PerfMonProducer = (function (_super) {
    __extends(PerfMonProducer, _super);
    function PerfMonProducer(sessionManager) {
        _super.call(this, sessionManager);
        this.nextChannelId = 0;
        this.simpleProducer = false;
    }
    PerfMonProducer.prototype.role = function () {
        return "producer";
    };

    PerfMonProducer.prototype.start = function () {
        this.dataSimulator = null;
        this.dataSimulator.on('metadata', this.onMetadata.bind(this));
        this.dataSimulator.on('data', this.onData.bind(this));
    };

    PerfMonProducer.prototype.stop = function () {
        if (this.dataSimulator)
            this.dataSimulator.stop();
        this.sessionManager = null;
    };

    PerfMonProducer.prototype.handleMessage = function (messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case 0:
                this.start();
                break;
            case 1:
                try  {
                    this.log("Describe Request: " + messageBody.uris);
                    for (var i = 0; i < messageBody.uris.length; i++) {
                    }
                    this.dataSimulator.describe(messageBody.uris, this.newChannelId.bind(this), this.sessionManager.store);
                } catch (exception) {
                    this.sessionManager.log(JSON.stringify(exception));
                    this.sessionManager.sendException(0, "Unable to describe URI " + messageBody.uris, messageHeader.messageId);
                }
                break;
            case 9:
                break;
            case 4:
                this.dataSimulator.startStreaming(messageBody.channels);
                break;
            case 5:
                this.dataSimulator.stopStreaming(messageBody.channels);
                break;
            default:
                _super.prototype.handleMessage.call(this, messageHeader, messageBody);
                break;
        }
    };

    /**
    * Handle channel data from the simulator, pass on to the consumer.
    * @param channelData
    */
    PerfMonProducer.prototype.onData = function (channelData) {
        var header = this.sessionManager.createHeader(1, 3, 0);
        this.sessionManager.send(header, channelData);
    };

    /** Handle new metadata coming from the simulator */
    PerfMonProducer.prototype.onMetadata = function (channelMetadata) {
        var header = this.sessionManager.createHeader(1, 2, 0);
        this.sessionManager.send(header, channelMetadata);
    };

    /** Generate a unique channel ID for this session */
    PerfMonProducer.prototype.newChannelId = function () {
        return this.nextChannelId++;
    };
    return PerfMonProducer;
})(Energistics.BaseProtocolHandler);
