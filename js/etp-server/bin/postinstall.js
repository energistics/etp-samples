var path = require('path');
var appRoot = path.dirname(path.dirname(module.filename));
var zlib = require('zlib');
var gzip = zlib.createGunzip();
var fs = require('fs');
var inp = fs.createReadStream(appRoot + '/logs/well(118b86a3-6518-40f8-b5c3-987228416dac)_wellbore(9267c9a2-c1a1-497a-8418-928d5053a90a)_log(1420MEOM59)_data.json.gz');
var out = fs.createWriteStream(appRoot + '/logs/well(118b86a3-6518-40f8-b5c3-987228416dac)_wellbore(9267c9a2-c1a1-497a-8418-928d5053a90a)_log(1420MEOM59)_data.json');
inp.pipe(gzip).pipe(out);
console.log("Unzipped sample data");