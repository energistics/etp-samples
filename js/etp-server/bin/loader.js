var fs = require('fs'),
    redis = require('redis'),
    xml2js = require('xml2js'),
    parser = new xml2js.Parser();

var redisClient = redis.createClient();

redisClient.on("error", function (err) {
    console.log("Error " + err);
});


var loadFile = function (path) {
	console.dir(path);
    fs.readFile(path, {encoding: 'utf8'}, function (err, data) {
		if(err && err.errno!=0) {
			console.dir(err);
		}
		else {
			if(data) {
				parser.parseString(data, function (err, result) {
					if (err && err.errno != 0) {
						console.dir(err);
					}
					else {
						if (result.logs != undefined) {
							var log = result.logs.log[0].$.uid;
							var rowCount = result.logs.log[0].logData[0].data.length;
							for (var i = 0; i < rowCount; ++i) {
								var values = result.logs.log[0].logData[0].data[i].split(",");
								for (var j = 1; j < values.length; j++) {
									if (values[j] !== "") {
										var key = log + i;
										redisClient.zadd(key, result.logs.log[0].logData[0].data[i], values[0]);
									}
									console.log(key + ":" + values[0]);
								}
								//console.dir(values);
							}
							//console.log(JSON.stringify(result));
							console.log('Loaded ' + log);
						}
						else {
							console.dir("logs found");
						}
					}
				});

				console.log(data.length);
			}
			else {
				console.log('data is undefined');
			}
		}
    });
}

String.prototype.endsWith = function (suffix) {
    return this.match(suffix + "$") == suffix;
};

var walk = require('walk'),
  fs = require('fs'),
  options,
  walker;

options = {
    followLinks: false
};

walker = walk.walkSync("h:/data/energyml/WITSMLSIG_SampleDataSet_V2", options);

// OR
// walker = walk.walkSync("/tmp", options);

walker.on("names", function (root, nodeNamesArray) {
    //console.log(root);
    for (var i = 0; i < nodeNamesArray.length; i++) {
        var file = nodeNamesArray[i];
        if (file.endsWith(".xml"))
            loadFile(root + "/" + file);
    }
});

walker.on("directories", function (root, dirStatsArray, next) {
  // dirStatsArray is an array of `stat` objects with the additional attributes
  // * type
  // * error
  // * name

  next();
});
