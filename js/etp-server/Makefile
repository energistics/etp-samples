export PATH := ./node_modules/.bin:$(PATH)
JSTESTS=$(wildcard ./test/*.js)
TSC=tsc
TSCFLAGS=--declaration --module commonjs --target ES5
TEST_RUNNER=mocha
TEST_RUNNER_ARGS=
BROWSER_MODULE=lib/browser/node-ralf.js
BROWSERIFY=browserify
MINIFY=uglifyjs

# shared between server and browser
SHARED_SOURCES= \
	lib/StoreSimulator.js \
	lib/JsonSimulator.js \
	lib/Energistics.js

CLIENT_SOURCES= $(SHARED_SOURCES) \
	web/MainWindow/MainWindow.js \
	web/logviewer/logviewer.js \
	web/logtransfer/logtransfer.js

# all server sources.
SERVER_SOURCES = $(SHARED_SOURCES) \
    lib/MongoStore.js \
	bin/server.js \
	bin/perfServer.js

%.js : %.ts
	$(TSC) $(TSCFLAGS) $<

build: $(SERVER_SOURCES) $(BROWSER_MODULE) 

$(BROWSER_MODULE): $(CLIENT_SOURCES)
	$(BROWSERIFY)  $(CLIENT_SOURCES) --outfile $@
	$(MINIFY) lib/browser/node-ralf.js --output lib/browser/node-ralf.min.js

browser_tests: $(JSTESTS)
	browserify $^ --outfile test/browser/tests.js

test:
	$(TEST_RUNNER) $(TEST_RUNNER_ARGS)

release: build
	npm pack

$(PROTOCOL_DEFINITION): $(SCHEMAS)
	node bin/genProtocol

all: build test release

.PHONY: test release
